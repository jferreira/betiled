#ifndef _SPRITE_H
#define _SPRITE_H

extern void __CALLEE__   clear_cursor();        // clear cursor
extern void __CALLEE__   print_cursor();        // print cursor at actx, acty
extern void __CALLEE__   update_cursor();       // clear cursor and print the cursor, ensures no flicker
#asm
.animframes
defw bitmap_1   ; 0
defw bitmap_1   ; 1
defw bitmap_1   ; 2
//.animframes_selection
defw bitmap_2   ; 3
defw bitmap_2   ; 4
defw bitmap_2   ; 5
defw bitmap_3   ; 6
defw bitmap_3   ; 7
defw bitmap_3   ; 8
defw bitmap_4   ; 9
defw bitmap_4   ; 10
defw bitmap_4   ; 11
defw bitmap_5   ; 12
defw bitmap_5   ; 13
defw bitmap_5   ; 14
defw bitmap_6   ; 15
defw bitmap_6   ; 16
defw bitmap_6   ; 17
// defw bitmap_7   ; 15
// defw bitmap_7   ; 16
// defw bitmap_7   ; 17

.bitmap_1
defb  85, 255, 255, 170, 255, 255, 255
defb  64, 233, 195, 170, 235, 195, 232
defb  84, 211, 243, 170, 251, 243, 214
defb  65, 215, 255, 170, 255, 255, 195
defb  84, 215,   0,   0,   0,  85, 214
defb  65, 215,   0,   0,   0,  85, 195
defb  81, 247,   0,   0,   0,  85, 243
defb  85, 255,   0,   0,   0,  85, 255
defb  85, 255,   0,   0,   0,  85, 255
defb  81, 247,   0,   0,   0,  85, 243
defb  84, 215,   0,   0,   0,  85, 214
defb  64, 215,   0,   0,   0,  85, 194
defb  65, 215, 255, 170, 255, 255, 195
defb  84, 211, 243, 170, 251, 243, 214
defb  65, 195, 195, 170, 235, 195, 195
defb  85, 255, 255, 170, 255, 255, 255

.bitmap_2
defb   0, 255, 255, 255,   0,   0,   0
defb   0, 254, 195, 247,   0, 255, 255
defb   0, 254, 195, 215,   0, 251, 214
defb   0, 235, 211, 247,   0, 255, 227
defb   0, 254, 215, 170,   0,  85, 227
defb   0, 235, 215,   0,   0,  85, 227
defb   0, 235, 247,   0,   0,  85, 251
defb   0, 255, 255,   0,   0,   0, 255
defb   0, 255, 255,   0,   0,   0, 255
defb   0, 235, 247,   0,   0,  85, 251
defb   0, 254, 215,   0,   0,  85, 227
defb   0, 254, 215, 255,   0,  85, 227
defb   0, 235, 211, 247,   0, 255, 227
defb   0, 254, 195, 215,   0, 251, 214
defb   0, 235, 195, 247,   0, 255, 255
defb   0, 255, 255, 255,   0,   0,   0

.bitmap_3
defb   0,   0, 235, 247, 170,   0,   0
defb   0,   0, 254, 211, 170,   0,   0
defb   0,   0, 254, 233, 170,   0, 255
defb   0,   0, 254, 211, 170,   0, 251
defb   0,   0, 254, 215, 170,   0, 235
defb   0,   0, 235, 215,   0,   0, 235
defb   0,   0, 235, 247,   0,   0, 251
defb   0,   0, 255, 255,   0,   0, 255
defb   0,   0, 255, 255,   0,   0, 255
defb   0,   0, 235, 247,   0,   0, 251
defb   0,   0, 254, 215,   0,   0, 235
defb   0,   0, 254, 253, 170,   0, 235
defb   0,   0, 254, 211, 170,   0, 251
defb   0,   0, 254, 195, 170,   0, 255
defb   0,   0, 235, 211, 170,   0,   0
defb   0,   0, 235, 247, 170,   0,   0

.bitmap_4
defb   0,   0,  85, 192, 253,   0,   0
defb   0,   0,  85, 252, 215,   0,   0
defb   0,   0,  85, 252, 215,   0,   0
defb   0,   0,  85, 233, 247,   0,   0
defb   0,   0,  85, 233, 215,   0,   0
defb   0,   0,  85, 195, 247,   0,   0
defb   0,   0,  85, 211, 247,   0,   0
defb   0,   0,  85, 255, 255,   0,   0
defb   0,   0,  85, 255, 255,   0,   0
defb   0,   0,  85, 212, 253,   0,   0
defb   0,   0,  85, 252, 215,   0,   0
defb   0,   0,  85, 252, 215,   0,   0
defb   0,   0,  85, 233, 247,   0,   0
defb   0,   0,  85, 233, 215,   0,   0
defb   0,   0,  85, 195, 247,   0,   0
defb   0,   0,  85, 211, 247,   0,   0

.bitmap_5
defb   0,   0,   0, 255, 227, 170,   0
defb   0,   0,   0, 251, 214, 170,   0
defb  85, 170,   0, 235, 252, 170,   0
defb  81, 170,   0, 251, 214, 170,   0
defb  65, 170,   0, 255, 214, 170,   0
defb  65, 170,   0,  85, 195, 170,   0
defb  81, 170,   0,  85, 227, 170,   0
defb  85, 170,   0,  85, 255, 170,   0
defb  85, 170,   0,  85, 255, 170,   0
defb  81, 170,   0,  85, 227, 170,   0
defb  65, 170,   0,  85, 214, 170,   0
defb  65, 170,   0, 255, 252, 170,   0
defb  81, 170,   0, 251, 214, 170,   0
defb  85, 170,   0, 235, 214, 170,   0
defb   0,   0,   0, 251, 195, 170,   0
defb   0,   0,   0, 255, 227, 170,   0

.bitmap_6
defb   0,   0,   0,  85, 255, 255, 170
defb  85, 255, 170,  85, 227, 214, 170
defb  84, 211, 170,  85, 195, 214, 170
defb  65, 247, 170,  85, 243, 195, 170
defb  65, 247,   0,  85, 255, 214, 170
defb  65, 247,   0,   0,  85, 195, 170
defb  81, 255,   0,   0,  85, 227, 170
defb  85, 170,   0,   0,  85, 255, 170
defb  85, 170,   0,   0,  85, 255, 170
defb  81, 255,   0,   0,  85, 227, 170
defb  65, 247,   0,   0,  85, 214, 170
defb  65, 247,   0,  85, 255, 214, 170
defb  65, 247, 170,  85, 243, 195, 170
defb  84, 211, 170,  85, 195, 214, 170
defb  85, 255, 170,  85, 227, 195, 170
defb   0,   0,   0,  85, 255, 255, 170

// .bitmap_7
// defb   1,   3,   3,   2,   3,   3,   3
// defb  64, 173,  15,   2,   7,  15, 232
// defb  84,  79, 207,   2,  71, 207,  94
// defb   5,  11,   3,   2,   3,   3,  15
// defb  84,  11,   0,   0,   0,   1,  94
// defb   5,  11,   0,   0,   0,   1,  15
// defb  69, 139,   0,   0,   0,   1, 207
// defb   1,   3,   0,   0,   0,   1,   3
// defb   1,   3,   0,   0,   0,   1,   3
// defb  69, 139,   0,   0,   0,   1, 207
// defb  84,  11,   0,   0,   0,   1,  94
// defb  64,  11,   0,   0,   0,   1,  74
// defb   5,  11,   3,   2,   3,   3,  15
// defb  84,  79, 207,   2,  71, 207,  94
// defb   5,  15,  15,   2,   7,  15,  15
// defb   1,   3,   3,   2,   3,   3,   3

// ;;;;;; Save vram_addr in a variable and restore hl in the clear_cursor !!!!!!!!!
// ; HL = vram_addr
// ; HLe = buffer
// ; DEe = bitmap
// ; BCe = mask table
// .__print_cursor
// 	ex de,hl
// 	ld (_vram_addr_save),hl
//
//     exx
//
//  	ld hl,animframe+1
//  	ld a,(hl)
//  	cp 17
//  	jr nz,next_frame
//     ld (hl),$ff
// .next_frame
//     inc (hl)
//
// .animframe
//     ld a,0
//     add a
//     ld c,a
//     ld b,0
// .set_animframes
//     ld hl,animframes
//     add hl,bc
//
//     ld a,(hl)
//     inc hl
//     ld h,(hl)
//     ld l,a
//
//     ex de,hl
//
// 	ld hl,buffer
//     ld b,MASKTBL/256
//
//     exx
//
//     call print_line
//     call print_line
//     call print_line
//     call print_line
//     call print_line
//     call print_line
//     call print_line
//     call print_line
//     call noline
//     call print_line
//     call print_line
//     call print_line
//     call print_line
//     call print_line
//     call print_line
//     call print_line
// 
// .print_line
//     ld b,7
// .print_line_loop    
//     ld a,(hl)          ; 1st byte
// 	exx
// 	ld (hl),a          ; put background in buffer
// 	ld a,(de)          ; 1st byte bitmap
// 	inc de
// 	ld c,a
// 	ld a,(bc)          ; a = mask 1st byte
// 	and (hl)           ; a = mask & background
// 	or c               ; apply sprite data
// 	inc l              ; use inc l instead of inc hl because buffer is at 256byte border
// 	exx
// 	ld (hl),a
// 	inc hl
// 	djnz print_line_loop
// 
// 	ld bc,$07f9    ; next scanline
// 	add hl,bc
// 	jp nc,end_line_sprite
// 
// 	ld bc,$c03c    ; next character
// 	add hl,bc
// 
// .end_line_sprite
// 	ret

;;;;;; Save vram_addr in a variable and restore hl in the clear_cursor !!!!!!!!!
; HL = vram_addr
; DE = bitmap
; BC = calculations
; HLe = buffer
; DEe = sel table
; BCe = mask table
.__print_cursor

 	ld hl,animframe+1
 	ld a,(hl)
 	cp 17
 	jr nz,next_frame
    ld (hl),$ff
.next_frame
    inc (hl)

.animframe
    ld a,0
    add a
    ld c,a
    ld b,0
.set_animframes
    ld hl,animframes
    add hl,bc

    ld a,(hl)
    inc hl
    ld h,(hl)
    ld l,a

	ex de,hl                   ; DE = bitmap

	ld (_vram_addr_save),hl    ; HL = vram_addr

    exx

    ld a,(_cursor_selected)
    or a
    jr z,_set_not_selected
    ld hl,_selected
    jr _next
._set_not_selected    
    ld hl,_not_selected

._next
    ld (_selection+1),hl

    ld hl,buffer
    ld b,MASKTBL/256
    ld d,SELTBL/256

    exx

    call print_line
    call print_line
    call print_line
    call print_line
    call print_line
    call print_line
    call print_line
    call print_line
    call noline
    call print_line
    call print_line
    call print_line
    call print_line
    call print_line
    call print_line
    call print_line

.print_line
    ld b,7
.print_line_loop
    ld a,(hl)          ; 1st byte

    exx
	ld (hl),a          ; put background in buffer
	exx

    ld a,(de)          ; 1st byte bitmap
	inc de

	exx

._selection
    jp _not_selected
._selected
    ld e,a
	ld a,(de)

._not_selected
	ld c,a
	ld a,(bc)          ; a = mask 1st byte
	and (hl)           ; a = mask & background
	or c               ; apply sprite data
	inc l              ; use inc l instead of inc hl because buffer is at 256byte border
	exx
	ld (hl),a
	inc hl
	djnz print_line_loop

	ld bc,$07f9    ; next scanline
	add hl,bc
	jp nc,end_line_sprite

	ld bc,$c03c    ; next character
	add hl,bc

.end_line_sprite
	ret

;-------------------------------------------------------------------------------
.noline
	ld bc,$3800
	add hl,bc
	jp nc,noline_end
	ld bc,$c03c
	add hl,bc

.noline_end
    ret

;-------------------------------------------------------------------------------
; HL = vram_addr
; HLe = buffer
._clear_cursor
    ld hl,buffer
    ld de,(_vram_addr_save)

    call clear_line
    call clear_line
    call clear_line
    call clear_line
    call clear_line
    call clear_line
    call clear_line
    call clear_line
    ex de,hl
    call noline
    ex de,hl
    call clear_line
    call clear_line
    call clear_line
    call clear_line
    call clear_line
    call clear_line
    call clear_line

.clear_line
    ldi            ; 1st byte
    ldi            ; 2nd
    ldi            ; 3rd
    ldi            ; 4th
    ldi            ; 5th
    ldi            ; 6th
    ldi            ; 7th

	ex de,hl
    ld bc,$07f9    ; next scanline
	add hl,bc
	jp nc,end_clear_line

	ld bc,$c03c    ; next character
	add hl,bc

.end_clear_line
    ex de,hl
	ret


;-------------------------------------------------------------------------------
._update_cursor

    ld hl,counti+1
// Change to move down the screen
    ld a,4

._sync
    cp (hl)
    jr nz,_sync

;    ld b,80
;._loop_dummy
;    djnz _loop_dummy

;    call _clear_cursor

    jp _cambiante

;-------------------------------------------------------------------------------
._print_cursor
    ld hl,_actx
	ld a,(hl)
	inc a
	inc a
	ld h,0
	ld l,a
	push hl

	ld hl,_acty
	ld a,(hl)
	ld bc,15
	ld h,0
	ld l,a
	add hl,bc
	push hl

	call _vram_addr
    jp __print_cursor
#endasm

#endif
# Betiled (CPC)

Betiled Amstrad CPC port of ZX Spectrum original version by Benway, https://computeremuzone.com/ficha.php?id=19

It is shared as an historic curiosity without any support, hoping that it may be interesting to the retro community.

## Requirements

- A POSIX environment. Make and split are used in the build.
- [Z88DK Z80 Development Kit](https://github.com/z88dk/z88dk/wiki/installation)
- [Pasmo Assembler](https://pasmo.speccy.org)
- [iDSK DSK tool](https://github.com/cpcsdk/idsk)

Once all requirements are met, run `make`.

At this point `betiled.dsk` should be ready to load in your emulator.

## Screenshots

<img src="media/loading_screen.jpg" alt="title" width="400"/> <img src="media/ingame.jpg" alt="in game 1" width="400"/> 


## Acknowledgments

- Thanks to David DONAIRE SANCHEZ aka DadMan for his graphics and ideas to improve the port.
- Thanks to WYZ for his WYZPlayer
- Thanks to CEZ team for their support during the development.


#ifndef _WYZ_H
#define _WYZ_H

#define MUSIC_GAMEOVER 6
#define MUSIC_MENU 7
#define MUSIC_THEEND 8
#define MUSIC_INGAME 9

#define FX_FLIP 0
#define FX_FLOP 1
#define FX_GET 2
#define FX_ITEM 3
#define FX_LINE 4
#define FX_45 5
#define FX_COMBO 6
#define FX_PAUSA 7
#define FX_SPIR 8
#define FX_TECLA 9
#define FX_TIMEATTACK 10

extern void __FASTCALL__ wyz_load(uchar song);
extern void __FASTCALL__ wyz_fx(uchar fx);
extern void __CALLEE__   wyz_play(void);
extern void __CALLEE__   wyz_pause(void);
extern void __CALLEE__   wyz_start(void);
extern void __CALLEE__   wyz_reset(void);

#asm
;CARGA UNA CANCION
;IN [A]=N� DE CANCION
._wyz_load
    ld a,l
    ld hl,tabla_song
    call ext_word

	ld de,buffer_dec
    call uncrunch

    ld hl,buffer_dec

    ; First data byte = tempo

    ld a,(hl)
    ld (tempo),a
    inc hl

    ld (canal_a),hl
    ld (puntero_a),hl

._wyz_load_A
    ld a,(hl)
    inc hl
    cp $3f
    jr nz,_wyz_load_A

    ld (canal_b),hl
    ld (puntero_b),hl

._wyz_load_B
    ld a,(hl)
    inc hl
    cp $3f
    jr nz,_wyz_load_B

    ld (canal_c),hl
    ld (puntero_c),hl

._wyz_load_C
    ld a,(hl)
    inc hl
    cp $3f
    jr nz,_wyz_load_C

    ld (canal_p),hl
    ld (puntero_p),hl

    ; Pauta should be initialized from data

    ld hl,interr        ; carga cancion
    set 1,(hl)          ; reproduce cancion
    xor a
    ld (ttempo),a

    ret

;--------------------------------------------------------------------------------------------------------------
._wyz_play
    ld hl,interr        ; play bit 1 on?
    bit 1,(hl)
    jr z,_wyz_play_effect

; tempo
    ld hl,ttempo        ; contador tempo
    inc (hl)
    ld a,(tempo)
    cp (hl)
    jr nz,pautas
    ld (hl),0

;interpreta
    ld iy,psg_reg+0
    ld ix,puntero_a
    ld bc,psg_reg+8
    call localiza_nota

    ld iy,psg_reg+2
    ld ix,puntero_b
    ld bc,psg_reg+9
    call localiza_nota

    ld iy,psg_reg+4
    ld ix,puntero_c
    ld bc,psg_reg+10
    call localiza_nota

    ld ix,puntero_p     ; el canal de efectos enmascara otro canal
    call localiza_efecto

.pautas
    ld iy,psg_reg+0
    ld ix,puntero_p_a
    ld hl,psg_reg+8
    call pauta          ; pauta canal A

    ld iy,psg_reg+2
    ld ix,puntero_p_b
    ld hl,psg_reg+9
    call pauta          ; pauta canal B

    ld iy,psg_reg+4
    ld ix,puntero_p_c
    ld hl,psg_reg+10
    call pauta          ; pauta canal C
    
;--------------------------------------------------------------------------------------------------------------
._wyz_play_effect
    ld hl,interr
    bit 2,(hl)          ; esta activado el efecto?
    jr z,_wyz_play_fx
    ld hl,(puntero_sonido)
    ld a,(hl)
    cp $ff
    jr z,fin_sonido
    ld (psg_reg+4),a
    inc hl
    ld a,(hl)
    rrca
    rrca
    rrca
    rrca
    and $0f
    ld (psg_reg+5),a
    ld a,(hl)
    and $0f
    ld (psg_reg+10),a
    inc hl
    ld a,(hl)
    and a
    jr z,no_ruido
    ld (psg_reg+6),a
    ld a,8+16
    jr si_ruido

.no_ruido
    ld a,8+16+32

.si_ruido
    ld (psg_reg+7),a
    inc hl
    ld (puntero_sonido),hl
    jr _wyz_play_fx

.fin_sonido
    ld hl,interr
    res 2,(hl)

.fin_nonplayer
    xor a
    ld (psg_reg+4),a
    ld (psg_reg+5),a
    ld a,8+16+32
    ld (psg_reg+7),a

;--------------------------------------------------------------------------------------------------------------
._wyz_play_fx
;reproduce efectos canal c
.reproduce_efecto
    ld hl,interr
    bit 5,(hl)      ; esta activado el efecto?
    jr z,rout
    ld hl,(puntero_efecto_c)
    ld a,(hl)
    cp $ff
    jr z,fin_efecto_c
    ld (psg_reg+4),a
    inc hl
    ld a,(hl)
    rrca
    rrca
    rrca
    rrca
    and $0f
    ld (psg_reg+5),a
    ld a,(hl)
    and $0f
    ld (psg_reg+10),a
    inc hl
    ld (puntero_efecto_c),hl
    jr rout

.fin_efecto_c
    ld hl,interr
    res 5,(hl)
    xor a
    ld (psg_reg+4),a
    ld (psg_reg+5),a
    ld (psg_reg+10),a


;--------------------------------------------------------------------------------------------------------------
.rout
    xor a
    ld hl,psg_reg

.lout
    call writepsghl
    inc a
    cp 13
    jr nz,lout
    ld a,(hl)
    and a
    ret z
    ld a,13
    call writepsghl
    xor a
    ld (psg_reg+13),a
    ret

;; a = register
;; (hl) = value
writepsghl:
    ld b,$f4
    out (c),a
    ld bc,$f6c0
    out (c),c
    defb $ed
    defb $71
    ld b,$f5
    outi
    ld bc,$f680
    out (c),c
    defb $ed
    defb $71
    ret

;--------------------------------------------------------------------------------------------------------------
._wyz_fx
    ld a,l
    ld hl,tabla_efectos
    call ext_word
    ld (puntero_efecto_c),hl
    ld hl,interr
    set 5,(hl)
    ld a,8+16+32
    ld (psg_reg+7),a
    ret

;--------------------------------------------------------------------------------------------------------------
._wyz_pause
    ld hl,interr
    res 1,(hl)
    res 2,(hl)
    ret

;--------------------------------------------------------------------------------------------------------------
._wyz_start
    ld hl,interr
    set 1,(hl)
    set 2,(hl)
    ret

;--------------------------------------------------------------------------------------------------------------
._wyz_reset
    xor a
    ld b,15
    ld hl,psg_reg

._wyz_reset_loop
    ld (hl),a
    inc hl
    djnz _wyz_reset_loop

    ld a,8+16+32
    ld (psg_reg+7),a

    jr rout


;--------------------------------------------------------------------------------------------------------------
;LOCALIZA NOTA CANAL A
;IN [PUNTERO_A]
; 00xxxxxx NOTA
; 01xxxxxx INSTRUMENTO
; 10xxxxxx EFECTO
; 11xxxxxx ENV
; iy psg_reg freq
; ix puntero canal
; bc psg_reg vol
.localiza_nota
    ld l,(ix+0)         ; hl=(puntero_a_c_b)
    ld h,(ix+1)
    ld a,(hl)
    and $c0             ; comando?
    jr z,lnjp0          ; es una nota

;BIT[0]=INSTRUMENTO
.comandos
    cp $40              ; instrumento
    jr nz,com_efecto
    ld a,(hl)
    and $3f             ; numero pauta
    inc hl
    ld (ix+0),l
    ld (ix+1),h
    ld hl,tabla_pautas
    call ext_word
    ld (ix+18),l
    ld (ix+19),h
    ld (ix+12),l
    ld (ix+13),h
    ld l,c
    ld h,b
    res 4,(hl)          ; apaga efecto envolvente
    xor a
    ld (psg_reg+13),a
    jr localiza_nota

.com_efecto
    cp $80
    jr nz,com_envolvente

    ld a,(hl)
    and $3f             ; numero efecto
    inc hl
    ld (ix+0),l
    ld (ix+1),h
    call inicia_sonido
    ret

.com_envolvente
    ld a,(hl)
    and $3f             ; numero efecto
    inc hl
    ld (ix+0),l         
    ld (ix+1),h
    ld l,c
    ld h,b
    set 4,(hl)          ; enciende efecto envolvente ******* temporal
    jr no_silencio_a

.lnjp0
    ld a,(hl)
    inc hl
    cp $3f
    jr nz,no_fin_canal_a
    ld l,(ix+6)         ; hl=(canal_a_b_c) reinicia canal
    ld h,(ix+7)
    ld (ix+0),l
    ld (ix+1),h
    jr localiza_nota

.no_fin_canal_a
    ld (ix+0),l         ; (puntero_a_b_c)=hl guarda puntero
    ld (ix+1),h
    and a               ; no reproduce nota si nota=0
    jr z,fin_rutina
    cp $01
    jr nz,no_silencio_a
    xor a
    ld (bc),a
    ld (psg_reg+13),a   ; envolvente off
    call nota
    ret

.no_silencio_a
    call nota           ; reproduce nota

    ld l,(ix+18)        ; hl=(puntero_p_a0) resetea pauta
    ld h,(ix+19)
    ld (ix+12),l        ; (puntero_p_a)=hl
    ld (ix+13),h

.fin_rutina
    ret

;--------------------------------------------------------------------------------------------------------------
;LOCALIZA EFECTO
;IN HL=[PUNTERO_P]
.localiza_efecto
    ld l,(ix+0)         ; hl=(puntero_p)
    ld h,(ix+1)
    ld a,(hl)
    and $c0
    cp $80
    jr nz,lejp0

    ld a,(hl)
    and $3f
    inc hl
    ld (ix+0),l
    ld (ix+1),h
    call inicia_sonido
    ret

.lejp0
    ld a,(hl)
    and $3f
    cp $3f
    inc hl
    jr nz,no_fin_canal_p
    ld l,(ix+2)         ; hl=(canal_p) reinicia canal
    ld h,(ix+3)
    ld (ix+0),l
    ld (ix+1),h
    jr localiza_efecto

.no_fin_canal_p
    ld (ix+0),l         ; (puntero_a_b_c)=hl guarda puntero
    ld (ix+1),h
    ret

;--------------------------------------------------------------------------------------------------------------
; PAUTA DE LOS 3 CANALES
; IN[IX]PUNTERO DE LA PAUTA
;    [HL]REGISTRO DE VOLUMEN
;    [IY]REGISTROS DE FRECUENCIA
.pauta
    bit 4,(hl)          ; si la envolvente esta activada no actua pauta
    ret nz
    push hl
    ld l,(ix+0)
    ld h,(ix+1)
    ld a,(hl)

    bit 7,a             ; loop
    jr z,pcajp0
    and $0f             ; loop pauta (0,15)
    ld d,0
    ld e,a
    sbc hl,de
    ld a,(hl)

.pcajp0
    bit 6,a             ; octava -1
    jr z,pcajp1
    ld e,(iy+0)
    ld d,(iy+1)

    rr d
    rr e
    ld (iy+0),e
    ld (iy+1),d
    jr pcajp2

.pcajp1
    bit 5,a             ; octava +1
    jr z,pcajp2
    ld e,(iy+0)
    ld d,(iy+1)

    rl e
    rl d
    ld (iy+0),e
    ld (iy+1),d

.pcajp2
    inc hl
    ld (ix+0),l
    ld (ix+1),h
    pop hl
    and $0f
    ld (hl),a
    ret

;--------------------------------------------------------------------------------------------------------------
;NOTA  REPRODUCE UNA NOTA
;IN [A]=CODIGO DE LA NOTA
;   [IY]=REGISTROS DE FRECUENCIA
.nota
    ld l,c
    ld h,b
    bit 4,(hl)
    jr nz,evolventes
    ld b,a
    ld hl,datos_notas
    rlca                ; x2
    ld d,0
    ld e,a
    add hl,de
    ld a,(hl)
    ld (iy+0),a
    inc hl
    ld a,(hl)
    ld (iy+1),a
    ret

;--------------------------------------------------------------------------------------------------------------
;IN [A]=CODIGO DE LA ENVOLVENTE
;   [IY]=REGISTRO DE FRECUENCIA
.evolventes
    ld hl,datos_env
    ld e,a
    ld d,0
    add hl,de
    ld a,(hl)
    ld (psg_reg+11),a  ; Envelope fine freq
    ld a,$0c
    ld (psg_reg+13),a  ; envelope shape /////////   12 (Repeating)
    xor a
    ld (iy+0),a
    ld (iy+1),a
    ld (psg_reg+12),a  ; envelope coarse freq
    ret

;--------------------------------------------------------------------------------------------------------------
.ext_word
    add a               ; *2
    add l
    ld l,a
    jr nc,notcarry
    inc h
.notcarry
    ld a,(hl)
    inc hl
    ld h,(hl)
    ld l,a
    ret

;--------------------------------------------------------------------------------------------------------------
;INICIA EL SONIDO N� [A]
.inicia_sonido
    ld hl,tabla_sonidos
    call ext_word
    ld (puntero_sonido),hl
    ld hl,interr
    set 2,(hl)
    ret


;--------------------------------------------------------------------------------------------------------------
.puntero_efecto_c
    defw 0          ; puntero del sonido que se reproduce

.tabla_efectos
    defw fx_flip
    defw fx_flop
    defw fx_get
    defw fx_item
    defw fx_line
    defw fx_45
    defw fx_combo
    defw fx_pausa
    defw fx_spir
    defw fx_tecla
    defw fx_tattack

.fx_flip
    defb 72, 6
    defb 63, 7
    defb 54, 8
    defb 36, 6
    defb 54, 5
    defb 45, 7
    defb 36, 6
    defb 27, 4
    defb 45, 5
    defb 9, 4
    defb 255

.fx_flop
    defb 9, 6
    defb 45, 7
    defb 27, 8
    defb 36, 6
    defb 45, 5
    defb 54, 7
    defb 36, 6
    defb 54, 4
    defb 63, 5
    defb 72, 4
    defb 255


.fx_get
    defb 72, 7
    defb 11, 12
    defb 72, 9
    defb 11, 7
    defb 0, 0
    defb 0, 0
    defb 72, 7
    defb 11, 5
    defb 0, 0
    defb 255


.fx_item
    defb 18, 7
    defb 18, 12
    defb 9, 11
    defb 9, 10
    defb 36, 10
    defb 36, 11
    defb 27, 9
    defb 27, 8
    defb 18, 7
    defb 18, 8
    defb 18, 7
    defb 18, 6
    defb 0, 0
    defb 36, 5
    defb 36, 6
    defb 18, 6
    defb 18, 5
    defb 9, 5
    defb 9, 4
    defb 18, 3
    defb 18, 2
    defb 36, 4
    defb 36, 4
    defb 18, 3
    defb 18, 3
    defb 9, 2
    defb 9, 2
    defb 18, 2
    defb 18, 2
    defb 255


.fx_line
    defb 104, 25
    defb 32, 28
    defb 144, 10
    defb 50, 22
    defb 72, 8
    defb 144, 9
    defb 63, 7
    defb 18, 6
    defb 72, 7
    defb 144, 6
    defb 63, 5
    defb 18, 3
    defb 18, 4
    defb 72, 5
    defb 144, 4
    defb 63, 3
    defb 18, 3
    defb 255


.fx_45
    defb 180, 8
    defb 144, 10
    defb 72, 10
    defb 162, 6
    defb 180, 9
    defb 144, 12
    defb 72, 10
    defb 162, 6
    defb 216, 8
    defb 72, 9
    defb 36, 7
    defb 18, 6
    defb 36, 7
    defb 144, 6
    defb 36, 5
    defb 18, 3
    defb 18, 4
    defb 36, 5
    defb 144, 4
    defb 36, 3
    defb 18, 3
    defb 255


.fx_combo
    defb 32, 8
    defb 22, 9
    defb 23, 10
    defb 22, 8
    defb 54, 12
    defb 54, 11
    defb 54, 12
    defb 54, 9
    defb 23, 14
    defb 22, 14
    defb 23, 14
    defb 0, 0
    defb 54, 12
    defb 54, 11
    defb 109, 12
    defb 109, 11
    defb 0, 0
    defb 54, 12
    defb 54, 11
    defb 54, 12
    defb 0, 0
    defb 22, 11
    defb 23, 10
    defb 54, 10
    defb 54, 10
    defb 54, 9
    defb 54, 8
    defb 0, 0
    defb 0, 0
    defb 0, 0
    defb 109, 0
    defb 0, 0
    defb 54, 8
    defb 54, 9
    defb 54, 10
    defb 0, 0
    defb 22, 9
    defb 23, 7
    defb 54, 6
    defb 54, 5
    defb 54, 5
    defb 54, 5
    defb 0, 0
    defb 0, 0
    defb 0, 0
    defb 109, 0
    defb 0, 0
    defb 54, 5
    defb 54, 6
    defb 54, 7
    defb 0, 0
    defb 22, 7
    defb 23, 6
    defb 54, 5
    defb 54, 4
    defb 255


.fx_pausa
    defb 27, 10
    defb 38, 11
    defb 54, 12
    defb 38, 7
    defb 0, 0
    defb 36, 9
    defb 29, 12
    defb 36, 11
    defb 29, 6
    defb 0, 0
    defb 0, 0
    defb 36, 8
    defb 29, 9
    defb 36, 8
    defb 29, 5
    defb 0, 0
    defb 36, 4
    defb 29, 5
    defb 36, 4
    defb 29, 3
    defb 36, 2
    defb 255


.fx_spir
    defb 18, 8
    defb 31, 7
    defb 54, 11
    defb 58, 10
    defb 72, 12
    defb 67, 11
    defb 54, 12
    defb 49, 10
    defb 36, 11
    defb 31, 8
    defb 27, 10
    defb 27, 5
    defb 27, 7
    defb 27, 5
    defb 27, 3
    defb 27, 1
    defb 27, 1
    defb 27, 1
    defb 27, 1
    defb 27, 1
    defb 27, 1
    defb 27, 7
    defb 27, 6
    defb 27, 5
    defb 27, 4
    defb 27, 3
    defb 27, 1
    defb 27, 1
    defb 27, 1
    defb 27, 1
    defb 27, 1
    defb 27, 5
    defb 27, 4
    defb 27, 3
    defb 27, 2
    defb 27, 1
    defb 255


.fx_tecla
    defb 27, 7
    defb 28, 9
    defb 36, 10
    defb 37, 7
    defb 68, 5
    defb 255

.fx_tattack
    defb 22, 11
    defb 18, 13
    defb 22, 12
    defb 36, 11
    defb 40, 13
    defb 45, 11
    defb 49, 10
    defb 49, 11
    defb 54, 13
    defb 22, 11
    defb 18, 14
    defb 49, 13
    defb 49, 11
    defb 54, 12
    defb 36, 12
    defb 22, 11
    defb 18, 12
    defb 27, 12
    defb 31, 10
    defb 27, 11
    defb 36, 10
    defb 22, 9
    defb 18, 11
    defb 27, 9
    defb 31, 8
    defb 27, 9
    defb 36, 7
    defb 22, 7
    defb 18, 8
    defb 27, 6
    defb 0, 0
    defb 0, 0
    defb 27, 6
    defb 36, 6
    defb 22, 5
    defb 18, 5
    defb 27, 4
    defb 255

;--------------------------------------------------------------------------------------------------------------
; banco de instrumentos 2 bytes por int.
;[0](RET 2 OFFSET)
;[1][+-PITCH]

.tabla_pautas
    defw pauta_0
    defw pauta_1
    defw pauta_2
    defw pauta_3
    defw pauta_4
    defw pauta_5
    defw pauta_6
    defw pauta_7
    defw pauta_8
    defw pauta_9
    defw pauta_10
    defw pauta_11
    defw pauta_12
    defw pauta_13
    defw pauta_14
    defw pauta_15

.pauta_0 defb 9,10,11,11,11,10,10,9,9,9,8,$80+1
.pauta_1 defb $40+13,12,11,10,10,10,9,9,9,8,$80+1
.pauta_2 defb $40+7,$20+8,8,8,8,7,7,7,7,6,6,6,5,$80+1
.pauta_3 defb $40+12,$20+11,$40+11,$20+11,$40+11,$20+10,$40+9,$20+10,$40+8,$20+8,$40+7,$20+7,$40+6,$20+6,$40+5,$20+4,$80+1
.pauta_4 defb 13,13,13,12,12,12,11,11,11,10,10,10,9,$80+1
.pauta_5 defb 10,10,10,9,9,9,9,8,8,8,7,7,7,7,6,6,6,6,5,5,$80+1
.pauta_6 defb 13,12,11,10,9,$80+1
.pauta_7 defb $20+13,12,11,10,10,10,9,9,9,8,8,8,7,7,7,6,$80+1
.pauta_8 defb 8,9,9,8,8,7,7,6,6,5,$80+1
.pauta_9 defb 7,8,8,7,7,6,6,5,5,4,$80+1
.pauta_10 defb $40+10,$20+10,9,8,7,$80+2
.pauta_11 defb 9,8,7,6,5,4,4,$80+1
.pauta_12 defb $40+12,$20+12,11,9,4,$80+1
.pauta_13 defb $40+11,$20+13,$40+11,11,10,9,8,8,7,$80+1
.pauta_14 defb 39,72,7,5,1,0,37,68,4,2,134
.pauta_15 defb 36,9,5,6,6,129

;--------------------------------------------------------------------------------------------------------------
;DATOS DE LOS EFECTOS DE SONIDO
;EFECTOS DE SONIDO

.tabla_sonidos
    defw sonido0
    defw sonido1
    defw sonido2
    defw sonido3
    defw sonido4
    defw sonido5
    defw sonido6
    defw sonido7
    defw sonido8
    defw sonido9
    defw sonido10
    defw sonido11
    defw sonido12
    defw sonido13
    defw sonido14

.sonido0
    defb 104, 31, 0
    defb 209, 45, 0
    defb 255

.sonido1
    defb 177, 31, 0
    defb 98, 62, 0
    defb 242, 61, 0
    defb 131, 76, 0
    defb 255

.sonido2
    defb 104, 30, 0
    defb 0, 10, 2
    defb 255

.sonido3
    defb 177, 31, 0
    defb 0, 14, 11
    defb 0, 12, 9
    defb 0, 12, 4
    defb 0, 11, 2
    defb 0, 10, 1
    defb 255

.sonido4
    defb 0, 12, 1
    defb 255

.sonido5
    defb 177, 27, 0
    defb 65, 41, 0
    defb 98, 57, 0
    defb 255

.sonido6
    defb 32, 28, 0
    defb 0, 10, 2
    defb 255

.sonido7
    defb 0, 12, 1
    defb 255

.sonido8
    defb 32, 31, 0
    defb 0, 14, 16
    defb 0, 12, 14
    defb 0, 11, 13
    defb 0, 10, 13
    defb 0, 9, 4
    defb 0, 8, 3
    defb 0, 7, 3
    defb 255

.sonido9
 	defb 170,26,2
    defb 199,42,0
    defb 72,57,0
    defb 227,54,0
    defb 255

.sonido10
 	defb 118,27,0
    defb 157,25,1
    defb 41,24,1
    defb 180,7,2
    defb 116,6,2
    defb 64,4,2
    defb 255

.sonido11
    defb 0,11,1
    defb 255

.sonido12
    defb 170,26,2
    defb 199,42,0
    defb 72,57,0
    defb 227,54,0
    defb 255

.sonido13
    defb 118,27,0
    defb 157,25,1
    defb 41,24,1
    defb 180,7,2
    defb 116,6,2
    defb 64,4,2
    defb 255

.sonido14
    defb 0,11,1
    defb 255

;DATOS MUSICA
;******** TABLA DE DIRECCIONES DE ARCHIVOS MUS
.tabla_song
    defw song_0     ; MASA374
    defw song_1     ; MOD372
    defw song_2     ; INTER37
    defw song_3     ; TECH372
    defw song_4     ; LSD37
    defw song_7     ; TIR37
    defw song_5     ; LOTH37 (game over)
    defw song_6     ; BENINT37 (menu)
    defw song_7     ; TIR37 (the end)
    defw song_8     ; INGAME_DAD


; De las 6 octavas nos tenemos que quedar con 5, con la nueva estructura
; Octavas seleccionables  [2-6]
; tone = 12 * octave + (note + semitone) + 2
;         private int getNote(char p)
;         {
;             int value = 0;
;
;             switch (p)
;             {
;                 case 'C':
;                     value = 0;
;                     break;
;                 case 'D':
;                     value = 2;
;                     break;
;                 case 'E':
;                     value = 4;
;                     break;
;                 case 'F':
;                     value = 5;
;                     break;
;                 case 'G':
;                     value = 7;
;                     break;
;                 case 'A':
;                     value = 9;
;                     break;
;                 case 'B':
;                     value = 11;
;                     break;
;             }
;
;             return value;
;         }

.datos_notas
    defw 0,0
    defw 1080,1015,965,902,854,805,760,717,676,640,602,575
    defw 540,507,482,451,427,402,380,358,338,320,301,284
    defw 270,253,241,225,213,201,190,179,169,160,150,142
    defw 134,126,120,112,106,100,94,89,84,80,75,71
    defw 67,63,60,56,53,50,47,44,42,40,37,35
;    defw 33,31,29,28,26,24,23,21,20,19,18,17

;DATOS DE LAS ENVOLVENTES
;ZX/1.77
.datos_env
    defb 0,0,133,125,118,111,106,99
    defb 93,88,83,79,74,70,67,62
    defb 59,56,53,49,47,44,42,39
    defb 37,36,33,31,30,28,26,25
    defb 23,22,21,20,18,18,17,16
    defb 15,14,13,12,12,11,11,10
    defb 9,9,8,8,7,7,7,6
    defb 6,6,5,5,4,4,4,4
    defb 4

;MUSICA PRESENTACION
.song_0
binary "data/masa374.dat"

.song_1
binary "data/mod372.dat"

.song_2
binary "data/inter37.dat"

.song_3
binary "data/tech372.dat"

.song_4
binary "data/lsd37.dat"

.song_5
binary "data/loth37.dat"

.song_6
binary "data/benint37.dat"

.song_7
binary "data/tir37.dat"

.song_8
binary "data/bing.dat"

; VARIABLES

;INTERRUPTORES 1=ON 0=OFF
;BIT 0=CARGA CANCION ON/OFF
;BIT 1=PLAYER ON/OFF
;BIT 2=SONIDOS ON/OFF
;BIT 3=EFECTOS ON/OFF
.interr
    defb 0

;MUSICA **** EL ORDEN DE LAS VARIABLES ES FIJO ******

.tempo
    defb 0                ; tempo

.ttempo
    defb 0                ; contandor tempo

.puntero_a
    defw 0                ; puntero del canal A

.puntero_b
    defw 0                ; puntero del canal B

.puntero_c
    defw 0                ; puntero del canal C

.canal_a
    defw 0                ; Direccion de inicio de la musica A

.canal_b
    defw 0                ; Direccion de inicio de la musica B

.canal_c
    defw 0                ; Direccion de inicio de la musica C

.puntero_p_a
    defw 0                ; puntero pauta canal A

.puntero_p_b
    defw 0                ; puntero pauta canal B

.puntero_p_c
    defw 0                ; puntero pauta canal C

.puntero_p_a0
    defw 0                ; INI puntero pauta canal A

.puntero_p_b0
    defw 0                ; INI puntero pauta canal B

.puntero_p_c0
    defw 0                ; INI puntero pauta canal C

;CANAL DE EFECTOS - ENMASCARA OTRO CANAL

.puntero_p
    defw 0                ; puntero del canal de efectos

.canal_p
    defw 0                ; direccion de inicio de los efectos

.psg_reg
    defb 0,0,0,0,0,0,0,8+16+32,0,0,0,0,0,0,0

;EFECTOS DE SONIDO

.n_sonido               ; numero de sonido
    defb 0

.puntero_sonido
    defw 0                ; puntero del sonido que se reproduce

;EFECTOS
.n_efecto
    defb 0                ; numero de sonido

.puntero_efecto
    defw 0                ; puntero del sonido que se reproduce

.buffer_dec
    defs 1872

.uncrunch_buffer
._hand_pointer_0
    defs 78
._hand_pointer_1
    defs 78
._hand_pointer_2
    defs 78
._hand_pointer_3
    defs 78
    
    defs 136

#endasm

#endif
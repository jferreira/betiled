#include "lib.h"
#include "sprite.h"
#include "tiles.h"
#include "fonts.h"
#include "wyz.h"
#include "text.h"

#define DIMGRID 8
#define GRIDSIZE DIMGRID*DIMGRID

extern uchar board[];
extern uchar menu[];
extern uchar grid[];
#asm
._board
    BINARY "data/board.bin"

._menu
    BINARY "data/menu.bin"

._grid
    defs 64
#endasm


// pal_game defines initial the game pallete and it contains the fade sequence
uchar pal_game[] = {0x54, 0x4d, 0x56, 0x4f, 0x4c, 0x4e, 0x52, 0x5c, 0x43, 0x55, 0x44, 0x40, 0x46, 0x53, 0x4b, 0x54,
                    0x54, 0x45, 0x54, 0x47, 0x5c, 0x4c, 0x56, 0x54, 0x4a, 0x38, 0x54, 0x5e, 0x56, 0x42, 0x38, 0x54,
                    0x54, 0x4c, 0x54, 0x4e, 0x54, 0x5c, 0x54, 0x54, 0x4e, 0x54, 0x54, 0x5c, 0x54, 0x52, 0x4a, 0x54,
                    0x54, 0x5c, 0x54, 0x4c, 0x54, 0x54, 0x54, 0x54, 0x4c, 0x54, 0x54, 0x54, 0x54, 0x56, 0x4e, 0x54,
                    0x54, 0x54, 0x54, 0x5c, 0x54, 0x54, 0x54, 0x54, 0x5c, 0x54, 0x54, 0x54, 0x54, 0x54, 0x4c, 0x54,
                    0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x5c, 0x54};
uchar pal_black[] ={0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54, 0x54};

uchar pal_inter[] ={0x43, 0x5c, 0x4e, 0x54,
                    0x4a, 0x54, 0x4c, 0x54,
                    0x4e, 0x54, 0x5c, 0x54,
                    0x4c, 0x54, 0x54, 0x54,
                    0x5c, 0x54, 0x54, 0x54,
                    0x54, 0x54, 0x54, 0x54};

uchar n,m,i,j;
uchar done;
uchar tx, ty;
uchar actx, acty;
uchar objx, objy;
uchar swaping;
uchar fireReleased;
uchar game_mode;
uchar tileset;
uchar time,ticks,timeindic, timeR;
uint  score, hiscore;
uchar tilesD[7];
uchar objetivo, doble;
uchar checkend;
uchar keyPressed;
uchar isPaused;
uchar s_tattack;
uchar ar, ab, iz, de;
uchar locker;
uchar cursor_selected;
uchar menu_state, cur_menu_state;
uint  timer_state;
uchar cambiante_flag;

extern uchar frameno;

extern void my_ISR();
extern void my_ISR_menu();
extern void my_ISR_game();
extern void print_menu();

extern uchar menu_pointer_pos;
extern uchar menu_pointer_offset;

#asm

; in: HL
; de: position
._print_highscore_number
    push de
    call _uint2dec
    ld hl,_score_init
    pop de
    push de
    call _put_string
    ld hl,_uint2dec_str
    pop de
    inc de
    inc de
    jp _put_string
    ;ret

._print_menu
    ld de,$e2d0+32/2    ; vram_addr(32,100);
    ld hl,_start_str
    call _put_string

    ld de,$e348+32/2    ; vram_addr(32,116);
    ld hl,_mode_str
    jp _put_string

._print_highscore
    ld de,$ca58+32/2    ; vram_addr(32,80);
    ld hl,_header_high_score
    call _put_string
    ld de,$fa94+34/2    ; vram_addr(32,102)
    call _put_string
    ld de,$cb0c+34/2    ; vram_addr(32,112)
    call _put_string
    ld de,$db48+34/2    ; vram_addr(32,122)
    call _put_string
    ld de,$eb84+34/2    ; vram_addr(32,132)
    call _put_string
    ld de,$f3c0+34/2    ; vram_addr(32,142)
    call _put_string


    ld hl,(_highs)
    ld de,$fa94+18/2+18
    call _print_highscore_number

    ld hl,(_highs+2)
    ld de,$cb0c+18/2+18
    call _print_highscore_number

    ld hl,(_highs+4)
    ld de,$db48+18/2+18
    call _print_highscore_number

    ld hl,(_highs+6)
    ld de,$eb84+18/2+18
    call _print_highscore_number

    ld hl,(_highs+8)
    ld de,$f3c0+18/2+18
    jp _print_highscore_number

//     ld hl,(_highs)
//     call _uint2dec
//     ld hl,_score_init
//     ld de,$fa94+18/2+18
//     call _put_string
//     ld hl,_uint2dec_str
//     ld de,$fa94+18/2+20
//     call _put_string
// 
//     ld hl,(_highs+2)
//     call _uint2dec
//     ld hl,_score_init
//     ld de,$cb0c+18/2+18
//     call _put_string
//     ld hl,_uint2dec_str
//     ld de,$cb0c+18/2+20
//     call _put_string
// 
//     ld hl,(_highs+4)
//     call _uint2dec
//     ld hl,_score_init
//     ld de,$db48+18/2+18
//     call _put_string
//     ld hl,_uint2dec_str
//     ld de,$db48+18/2+20
//     call _put_string
// 
//     ld hl,(_highs+6)
//     call _uint2dec
//     ld hl,_score_init
//     ld de,$eb84+18/2+18
//     call _put_string
//     ld hl,_uint2dec_str
//     ld de,$eb84+18/2+20
//     call _put_string
// 
//     ld hl,(_highs+8)
//     call _uint2dec
//     ld hl,_score_init
//     ld de,$f3c0+18/2+18
//     call _put_string
//     ld hl,_uint2dec_str
//     ld de,$f3c0+18/2+20
//     jp _put_string

._my_ISR_menu
    push af
    ex af,af
    push af
    push bc
    push de
    push hl
    exx
    push bc
    push de
    push hl
    push ix
    push iy

    ld hl,counti+1
    ld b,$f5
    in a,(c)
    rra
    jp nc,nexti
    ld (hl),$ff            ; In VBL happens the interrupt counter will contain a zero

.nexti
    inc (hl)

.counti
    ld a,0
    cp 6
    jr c,ok
    xor a

.ok
    add a
    ld c,a
    ld b,0
.isr_vector_table
    ld hl,tabint_menu
    add hl,bc

    ld a,(hl)
    inc hl
    ld h,(hl)
    ld l,a
    ld bc,exint
    push bc
    jp (hl)

.exint
    pop iy
    pop ix
    pop hl
    pop de
    pop bc
    exx
    pop hl
    pop de
    pop bc
    pop af
    ex af,af
    pop af

._my_ISR
    ei
    ret

.tabint_highscore
    defw int0_menu
    defw _wyz_play
    defw int2_menu
    defw int3a_menu
    defw int4_menu
    defw int5_menu

.tabint_menu
    defw int0_menu
    defw _wyz_play
    defw int2_menu
    defw int3_menu
    defw int4_menu
    defw int5_menu

;;---------------------------------------------------------------------------------------
.int0_menu
    ld bc,$7f04        ; #7fxx GATE ARRAY IO port, 4 colors
	ld hl,frameno
	ld a,(hl)
	and $7c
	rrca
	rrca
	ld hl,menu_pal1
	cp 8
	jp z,_set_pal_loop
	cp 10
    jp z,_set_pal_loop
    cp 11
    jp z,_set_pal_loop
    cp 13
    jp z,_set_pal_loop

; set default colors for top side of screen
    ld hl,menu_pal0
    jp _set_pal_loop

;;---------------------------------------------------------------------------------------
.int2_menu
    ld hl,(_timer_state)
    dec hl
    ld a,h
    or l
    ld (_timer_state),hl
    jr nz, _no_menu_state_change

    ; restart timer
    ld hl,500
    ld (_timer_state),hl

    ; change _menu_state
    ld hl,_menu_state
    or (hl)
    jr z,_set_menu_state_highscore
    dec (hl)

    ld hl,tabint_menu
    ld (isr_vector_table+1),hl
    ld hl,_my_ISR_menu
    call _set_ISR
    jp _no_menu_state_change

._set_menu_state_highscore
    inc (hl)
    ld hl,tabint_highscore
    ld (isr_vector_table+1),hl
    ld hl,_my_ISR_menu
    call _set_ISR


._no_menu_state_change
    call _get_keystate

    ld bc,$7f04        ; #7fxx GATE ARRAY IO port, 4 colors
    ld hl,menu_pal2
    jp _set_pal_loop

;;---------------------------------------------------------------------------------------
.int3a_menu
	ld hl,frameno
	inc (hl)
	ret

;;---------------------------------------------------------------------------------------
; Print hand cursor
.int3_menu
; The position of the cursor is controlled by the user input
; The offset is controlled by the frame counter
    ld hl,_menu_pointer_offset
    ld a,(hl)
    or a
    jr z,nooffset

    call get_menu_pointer_vram_pos
    ld hl,$c000
    call _put_tile_6x13

.nooffset
    ld hl,_menu_pointer_pos
    ld a,(hl)
    inc hl
    add (hl)
    ld (hl),0 ; reset offset
    dec hl
    ld (hl),a
    call get_menu_pointer_vram_pos_0

	ld hl,frameno
	ld a,(hl)
	inc (hl)
	and $c
	rrca
	ld hl,tabadsp
	ld b,0
	ld c,a
	add hl,bc
    ld a,(hl)
    inc hl
    ld h,(hl)
    ld l,a
    jp _put_tile_6x13

.get_menu_pointer_vram_pos
    ld hl,_menu_pointer_pos
    ld a,(hl)
.get_menu_pointer_vram_pos_0
    ld hl,menu_pointer_vram_pos
    add a
    ld b,0
    ld c,a
    add hl,bc
    ld e,(hl)
    inc hl
    ld d,(hl)
    ret

;;---------------------------------------------------------------------------------------
.int4_menu
    ld b,173
.paus1
    djnz paus1

    ld bc,$7f00               ;; 3
    out (c),c                 ;; 4
    ld hl,raster_colours1      ;; 3
    ld e,end_raster_colours1-raster_colours1   ;;2


.rl1
    ld a,(hl)                 ;; 2
    inc hl                    ;; 2
    out (c),a                 ;; 4

    ld d,13                   ;; 2
.paus2
    dec d                     ;; 1
    jr nz,paus2               ;; nc 2, c 3

    dec e                     ;; 1
    jp nz,rl1                 ;; 3

    ret

;;---------------------------------------------------------------------------------------
.int5_menu
; Occurs at ~230 and we start printing at 256 = 24 scanlines for sync + 1 for setting the color
    ld b,191
.paus3
    djnz paus3

    ld b,194
.paus4
    djnz paus4

;; select pen to change (colour 0)
    ld bc,$7f03               ;; 3
    out (c),c                 ;; 4

;; start of table for raster colours
ld hl,raster_colours      ;; 3
;; number of colours in the table
ld e,end_raster_colours-raster_colours   ;;2

.rl
;; get colour byte from table
    ld a,(hl)                 ;; 2
    inc hl                    ;; 2
;; output colour to hardware
    out (c),a                 ;; 4

;; delay so that the next colour change
;; occurs immediatly below this one
    ld d,13                   ;; 2
.paus5
    dec d                     ;; 1
    jr nz,paus5               ;; nc 2, c 3

    dec e                     ;; 1
    jp nz,rl                  ;; 3
    jp put_char_scroll


;;---------------------------------------------------------------------------------------

;; de = start of line to scroll
.scroll_line
    ld b,62
    ld h,d
    ld l,e

.scroll_line_0
    inc l
    ld a,(hl)
    and $88
    rrca
    rrca
    rrca
    ld c,a
    ld a,(de)
    and $77
    add a
    or c
    ld (de),a
    inc e
    djnz scroll_line_0
    ret

.put_char_scroll
    ei
	ld hl,frameno
	ld a,(hl)
	and $7
	jr nz,put_char_scroll_1

.put_char_scroll_0
    call put_char_scroll_1
    ld hl,char_pointer_scroll
    ld de,$c780+60
    ld a,(hl)
    inc (hl)

    ld l,a
    ld h,0
    ld bc,_scroll_str
    add hl,bc
    ld a,(hl)
    bit 7,a
    jp z,wrchar
    ld hl,char_pointer_scroll
    ld (hl),0
    ret

.put_char_scroll_1
    ld de,$c780
    call scroll_line
    ld de,$cf80
    call scroll_line
    ld de,$d780
    call scroll_line
    ld de,$df80
    call scroll_line
    ld de,$e780
    call scroll_line
    ld de,$ef80
    call scroll_line
    ld de,$f780
    call scroll_line

    ret

.char_pointer_scroll
    defb 0

._menu_pointer_pos
    defb 0

._menu_pointer_offset
    defb 0

.tabadsp
    defw _hand_pointer_0
    defw _hand_pointer_1
    defw _hand_pointer_2
    defw _hand_pointer_3

.menu_pointer_vram_pos
    defw $dad0+9
    defw $db48+9
    defw $dbc0+9

._frameno
.frameno
    defb 0

.menu_pal0
    defb $4F,$4D,$58,$54

.menu_pal1
    defb $4B,$4F,$5D,$54

.menu_pal2
    defb $4B,$4E,$46,$54

.raster_colours
defb $56,$5E,$52,$4A,$52,$5E,$56
.end_raster_colours

.raster_colours1
defb $53,$57,$57,$57,$57,$55,$57,$55,$55,$55,$55,$55,$55,$55,$55,$44,$55,$44,$44,$44,$44,$44,$54,$44,$54
.end_raster_colours1

;;---------------------------------------------------------------------------------------
.tabint_game
    defw _my_ISR+1      ; VBL
    defw _my_ISR+1
    defw _my_ISR+1
    defw int1_game
    defw _wyz_play
    defw _my_ISR+1

;;---------------------------------------------------------------------------------------
.int1_game
; manage time
    ld hl,_isPaused
    ld a,(hl)
    or a
    ret nz

 	ld hl,_ticks
 	ld a,(hl)
 	inc (hl)
 	cp 50
    ret nz
 	xor a
 	ld (hl),a

    ld hl,_time
    dec (hl)
    ld a,(hl)
    call _uchar2dec0
    ld de,$c81e
    ld hl,_uchar2dec_str
    jp _put_string0


;;---------------------------------------------------------------------------------------

#endasm


void show_score(void)
{

#asm
	ld hl,(_score)
	call _uint2dec
	ld de,$c803
	ld hl,_uint2dec_str
	call _put_string0

    or a                ; reset carry flag
    ld de,(_score)
    ld hl,(_hiscore)
//    ld hl,(_highs)
    sbc hl,de
    ex de,hl
    jr nc,no_update
    ld (_hiscore),hl
//    ld (_highs),hl
	call _uint2dec
	ld de,$c803+45
	ld hl,_uint2dec_str
	call _put_string0

.no_update
    call _vsync
	ld hl,_tilescore
	ld de,$ee90+2
	ex af,af
	ld a,7

.loop_show_tilescore1
	ex af,af
    call _put_tile_4x10
    ld bc,-($836)
    ex de,hl
    add hl,bc
    ex de,hl
    ex af,af
    dec a
    jr nz,loop_show_tilescore1
    ex af,af

    ld a,(_objetivo)  ; Self modifiying code to compare with objetivo
    ld (cmpobj+1),a
    ld hl,_checkend   ; Initializes checkend
    ld (hl),0
    ld de,_tilesD     ; Array that contains the no of line for every tile
    ld b,7            ; index counter

    exx
    ld de,$fecc
    exx

.loop_showscore_tilesD
    ld a,(de)
    inc de

.cmpobj
    cp 0
    jr nc,tile_ok

    exx
    inc de
    inc de
    call _uchar2dec0
    ld hl,_uchar2dec_str
    call _put_string0
    exx

    djnz loop_showscore_tilesD
    ret

.tile_ok
    inc (hl)          ; increment checkend

    ld a,b
    cpl
    and $7
    add a             ; a*2
    ld c,a
    add a             ; a*4
    add a             ; a*8
    sub c             ; a = a*8 - a*2

    exx
    push de
    ld e,a
    ld d,0
    ld hl,$ee92
    add hl,de
    ld de,_tick
    ld a,16
    call loop_tile_transparency
    pop de
    ld hl,6
    add hl,de
    ex de,hl
    exx

    djnz loop_showscore_tilesD
#endasm
}

uchar CheckPreLines(uchar draw)
{
	uchar ret = FALSE;
    uchar *tile = grid;

	for(n = 0; n < DIMGRID; n++){
		for(m = 0; m < DIMGRID; m++, tile++){
            if(*tile > 7){ // Si hay una ficha "especial" ya tenemos un movimiento posible
                *tile |= 0x80;
			    ret = TRUE;
            }

            done = FALSE;
			if (m < DIMGRID - 1 && *tile == *(tile + 1)){                       					// O O
			    if (m < DIMGRID - 3 && *tile == *(tile + 3)){                              			// O O X O
                    *(tile + 3) |= 0x80;
                    done = TRUE;
                }
                if (m > 1 && *tile == *(tile - 2)){                                        			// O X O O
                    *(tile - 2) |= 0x80;
                    done = TRUE;
                }
                if (n > 0 && m > 0 && *tile == *(tile - DIMGRID - 1)){                     			// ^ O O
                    *(tile - DIMGRID - 1) |= 0x80;
                    done = TRUE;
                }
                if (n > 0 && m < DIMGRID - 2 && *tile == *(tile - DIMGRID + 2)){           			// O O ^
                    *(tile - DIMGRID + 2) |= 0x80;
                    done = TRUE;
                }
                if (n < DIMGRID - 1 && m > 0 && *tile == *(tile + DIMGRID - 1)){           			// _ O O
                    *(tile + DIMGRID - 1) |= 0x80;
                    done = TRUE;
                }
                if (n < DIMGRID - 1 && m < DIMGRID - 2 && *tile == *(tile + DIMGRID + 2)){ 			// O O _
                    *(tile + DIMGRID + 2) |= 0x80;
                    done = TRUE;
                }
                if(done){
                    *tile |= 0x80;
                    *(tile+1) |= 0x80;
                    ret = TRUE;
                }

            }

            if(m < DIMGRID - 2 && *tile == *(tile + 2)) {                     					// O X O
                if(n > 0 && *tile == *(tile - DIMGRID + 1)){                              			// O ^ O
                    *tile |= 0x80;
                    *(tile+2) |= 0x80;
                    *(tile - DIMGRID + 1) |= 0x80;
                    ret = TRUE;
                }
                if(n < DIMGRID - 1 && *tile == *(tile + DIMGRID + 1)){                    			// O _ O
                    *tile |= 0x80;
                    *(tile+2) |= 0x80;
                    *(tile + DIMGRID + 1) |= 0x80;
                    ret = TRUE;
                }
            }

            done = FALSE;
            if(n < DIMGRID - 1 && *tile == *(tile + DIMGRID)) {              					// O O
                if(n < DIMGRID - 3 && *tile == *(tile + (3 * DIMGRID))){                   		// O O X O
                    *(tile + (3 * DIMGRID)) |= 0x80;
                    done = TRUE;
                }
                if(n > 1 && *tile == *(tile - (2 * DIMGRID))){                             		// O X O O
                    *(tile - (2 * DIMGRID)) |= 0x80;
                    done = TRUE;
                }
                if(m > 0 && n > 0 && *tile == *(tile - DIMGRID - 1)){                     			// < O O
                    *(tile - DIMGRID - 1) |= 0x80;
                    done = TRUE;
                }
                if(m > 0 && n < DIMGRID - 2 && *tile == *(tile + (2 * DIMGRID) - 1)){      		// O O <
                    *(tile + (2 * DIMGRID) - 1) |= 0x80;
                    done = TRUE;
                }
                if(m < DIMGRID - 1 && n > 0 && *tile == *(tile - DIMGRID + 1)){             		// > O O
                    *(tile - DIMGRID + 1) |= 0x80;
                    done = TRUE;
                }
                if(m < DIMGRID - 1 && n < DIMGRID - 2 && *tile == *(tile + (2 * DIMGRID) + 1 )){	// O O >
                    *(tile + (2 * DIMGRID) + 1 ) |= 0x80;
                    done = TRUE;
                }
                if(done){
                    *tile |= 0x80;
                    *(tile + DIMGRID) |= 0x80;
                    ret = TRUE;
                }
            }

            if(n < DIMGRID - 2 && *tile == *(tile + 2 * DIMGRID)) {          					// O X O
                if (m < DIMGRID - 1 && *tile == *(tile + DIMGRID + 1)){                    			// O > O
                    *tile |= 0x80;
                    *(tile + 2 * DIMGRID) |= 0x80;
                    *(tile + DIMGRID + 1) |= 0x80;
                    ret = TRUE;
                }
                if (m > 1 && *tile == *(tile + DIMGRID - 1)){                              			// O < O
                    *tile |= 0x80;
                    *(tile + 2 * DIMGRID) |= 0x80;
                    *(tile + DIMGRID - 1) |= 0x80;
                    ret = TRUE;
                }
            }
        }
    }

    for(n = 0, tile = grid; n < DIMGRID*DIMGRID; n++, tile++){
        if(!draw) {
            *tile &= 0x7f;
        }
        else {
            *tile ^= 0x80;
        }

    }
    return ret;
}

void CreaTile(uchar donde)
{
    if(game_mode == 1 || (((uchar)rand()) % 100) > 2 || (game_mode == 0 && objetivo < 8))
    {
        grid[donde] = (((uchar)rand() % 7));
        return;
    }

    grid[donde] = ((((uchar)rand()) % 7) * 4) + 0x8;

    // Cambiante
    if(grid[donde] & 0x20)
    {
        if(cambiante_flag == FALSE)
        {
            grid[donde] = (((uchar)rand()) % 7) + 0x20;
            cambiante_flag = TRUE;
        }
        else
        {
            grid[donde] = (((uchar)rand() % 7));
        }
    }
}

void reset_grid()
{
    uchar *tile;

    cambiante_flag = FALSE;

	for(n = 0; n < GRIDSIZE; n++)
    {
		CreaTile(n);
	}

    do{
		done = TRUE;
        for(n = 1; n < DIMGRID - 1; n++){
			for(m = 0; m < DIMGRID; m++) {
				tile = &grid[n * DIMGRID + m];
                if (*tile == *(tile - DIMGRID) && *tile == *(tile + DIMGRID)){
					if (++(*tile) > 6)
                       *tile = 0;
                    done = FALSE;
                }

                tile = &grid[m * DIMGRID + n];
                if (*tile == *(tile - 1) && *tile == *(tile + 1)){
                    if (++(*tile) > 6)
                       *tile = 0;
                    done = FALSE;
                }
            }
        }
    } while (!done || !CheckPreLines(0));
}

void Gravedad(void)
{

    uchar *tile;

    for(m = 0; m < DIMGRID; m++)
    {
        for(n = DIMGRID; n > 0; n--)
        {
            if(grid[(n - 1) * DIMGRID + m] & 0x80)
            {
                for(i = n - 1; i > 0; i--)
                {
                    grid[i * DIMGRID + m] = grid [(i - 1) * DIMGRID + m];
                }

                CreaTile(m);

                for(j = 2; j > 0; j--)
                {
                    for(i = n; i > 1; i--)
                    {
                        if(grid[(i - 1) * DIMGRID + m] & 0x80)
                        {
                            tile = &explosion4;
                        }
                        else
                        {
                            tile = tiles[grid[(i - 1) * DIMGRID + m] & 0x1f];
                        }
                        vram_addr(m * 12 + 4, (i - 1) * 24 - (j - 1)*12 + 15);
                        fast_put_tile(tile);
                    }

                    tile = tiles[grid[m] & 0x1f];
                    if(j == 2)
                    {
                        tile += 72;
                        vram_addr(m * 12 + 4, 15);
                        fast_put_half_tile(tile);
                    }
                    else
                    {
                        vram_addr(m * 12 + 4, 15);
                        fast_put_tile(tile);
                    }

                    wait(2);
                }

                n++;
            }
        }
    }
}

void explosion(uchar clear)
{
    uchar *tile;
    uchar *frame;

    for(n = 0; n < 5; n++){
        tile = grid;
        frame = explosion_seq[n];
        for(m = 0; m < GRIDSIZE; m++){
            if(*tile & 0x80 || clear){
                vram_addr((m % DIMGRID)*12 + 4, (m / DIMGRID)*24 + 15);
                //put_tile_6x24(frame);
                fast_put_tile(frame);
            }
            tile++;
        }
        if(clear == FALSE)
            wait(15);
    }

}



// Aunque con menor frecuencia, siguen produciendose explosiones con l�neas que tienen tiles especiales.
// Me he puesto a mirar el c�digo y he visto esta l�nea en la funci�n FindLines:
//
// if((*tile & 0x7f) < 8 && m < DIMGRID - 2 && (*tile & 0x7) == (*(tile + 1) & 0x7) && (*tile & 0x7) == (*(tile + 2) & 0x7))
//
//
// Esto parece ser el origen de todos los males, o al menos de parte de ellos, ya que no compruebas si todas
// las casillas para formar una l�nea son menores de 8, solo lo haces con el primer tile, por lo que los siguientes
// pueden dar coincidencias siendo tiles especiales, y no coinciden siempre porque dependiendo del estado de la
// animaci�n del tile especial, pues tendr� un n�mero distinto y cada tile especial coincidir� solo con
// determinados colores y solo 1 vez cada X estados.
//
// As� pues, me parece que deber�a ser m�s bien as�:
//
// if((*tile & 0x7f) < 8 && m < DIMGRID - 2 && (*tile & 0x7) == (*(tile + 1) & 0x7f) && (*tile & 0x7) == (*(tile + 2) & 0x7f))
//
// Voy a seguir mirando a ver si hay m�s l�neas de este tipo en la funci�n.
//
// PD: Efectivamente, aqu� tambi�n hay que corregirlo:
// if((*tile & 0x7f) < 8 && n < DIMGRID - 2 && (*tile & 0x7) == (*(tile + DIMGRID) & 0x7f) && (*tile & 0x7) == (*(tile + DIMGRID * 2) & 0x7f))
char FindLines(void)
{
    uchar lines = 0;
    uchar *tile = grid;
	static uchar prescore;

    for(n = 0; n < DIMGRID; n++)
    {
		for(m = 0; m < DIMGRID; m++, tile++)
        {
			if((*tile & 0x7f) < 8 && m < DIMGRID - 2 && (*tile & 0x7) == (*(tile + 1) & 0x7f) && (*tile & 0x7) == (*(tile + 2) & 0x7f))
            {
				if((*tile & 0x80) == 0 || (*(tile + 1) & 0x80) == 0 || (*(tile + 2) & 0x80) == 0)
                {
					lines++;
                    wyz_fx(FX_LINE);
                }

                i = 0;
                prescore = 0;
                while(m + i < DIMGRID && (*tile & 0x7) == (*(tile + i) & 0x7f))
                {
					if((*(tile + i) & 0x80) == 0)
                    {
						*(tile + i) |= 0x80;
						tilesD[((*tile) & 0x7)]++;
						prescore++;
                    }
                    i++;
                }

				if(prescore)
                {
					score += prescore - 2;

                    if(prescore > 3)
                    {
                        wyz_fx(FX_45);
                        #asm
                        ld de,uncrunch_buffer
                        ld hl,_linex
                        call uncrunch
                        ld hl,uncrunch_buffer
                        call printindic

                        ld a,(_st_FindLines_prescore)
                        call printindicnumber

                        ld b,3
                        call settimeindic
                        #endasm
                	}

                    if((*tile & 0x7) == doble)
                    {
                        score += prescore - 2;
                    }

                    if(++time > 80)
                    {
                        time = 80;
                    }
                }
            }

            if((*tile & 0x7f) < 8 && n < DIMGRID - 2 && (*tile & 0x7) == (*(tile + DIMGRID) & 0x7f) && (*tile & 0x7) == (*(tile + DIMGRID * 2) & 0x7f))
            {
                if((*tile & 0x80) == 0 || (*(tile + DIMGRID) & 0x80) == 0 || (*(tile + DIMGRID * 2) & 0x80) == 0)
                {
                    lines++;
					wyz_fx(FX_LINE);
                }

				i = 0;
                prescore = 0;
                while(n + i < DIMGRID && (*tile & 0x7) == (*(tile + i*DIMGRID) & 0x7f))
                {
					if((*(tile + i*DIMGRID) & 0x80) == 0)
                    {
						*(tile + i*DIMGRID) |= 0x80;
						tilesD[(*tile & 0x7)]++;
						prescore++;
                    }
                    i++;
                }

				if(prescore)
                {
					score += prescore - 2;
					if(prescore > 3)
                    {
                        wyz_fx(FX_45);
                        #asm
                        ld de,uncrunch_buffer
                        ld hl,_linex
                        call uncrunch
                        ld hl,uncrunch_buffer
                        call printindic
                        ld a,(_st_FindLines_prescore)
                        call printindicnumber

                        ld b,3
                        call settimeindic
                        #endasm
                    }

                    if((*tile & 0x7) == doble)
                    {
					    score += prescore - 2;
                    }

                    if(++time > 80)
                    {
                        time = 80;
                    }
               }
            }
        }
    }

	if(lines)
    {
	    explosion(FALSE);
	}

	return lines;
}

uchar SwapTile(uchar x, uchar y, char sent)
{
	static uchar combo, lines;
	uchar *tile,*tile2;

    tile = &grid[x + y * DIMGRID];

    clear_cursor();

    if(sent){
       wyz_fx(FX_FLIP);
       swap(tile, tile + sent);

       tile2 = tiles[*tile & 0x1f];
       vram_addr(actx + 4, acty + 15);
       put_tile_6x24(tile2);

       tile2 = tiles[*(tile+sent) & 0x1f];
       vram_addr(actx + 12 * (sent % DIMGRID) + 4 , acty + 24 * (sent / DIMGRID) + 15);
       put_tile_6x24(tile2);
    }

    combo = FindLines();

    if (combo == 0 && sent) {  // Si no has hecho l�neas, y es una jugada de moverse, no de cargarse un tile especial...
        wyz_fx(FX_FLOP);
        wait(60);
        swap(tile, tile + sent);

        tile2 = tiles[*tile & 0x1f];
        vram_addr(actx + 4, acty + 15);
        put_tile_6x24(tile2);

        tile2 = tiles[*(tile+sent) & 0x1f];
        vram_addr(actx + 12 * (sent % DIMGRID) + 4, acty + 24 * (sent / DIMGRID) + 15);
        put_tile_6x24(tile2);

        print_cursor();
        return;
    }

    do{
        show_score();
        Gravedad();
        lines = FindLines();
        combo += lines;
        if (combo > 1){
            #asm
                ld de,uncrunch_buffer
                ld hl,_combox
                call uncrunch
                ld hl,uncrunch_buffer
                call printindic

                ld a,(_st_SwapTile_combo)
                call printindicnumber

                ld b,3
                call settimeindic

            #endasm
            score += combo * 2;
            show_score();
        }
    } while(lines);


    if(combo > 1){
         wyz_fx(FX_COMBO);
    }

// #asm
//     ld	a,(_st_SwapTile_combo)
//     cp #2
//     jr c, _SwapTile1
//     ld hl,6
//     call _wyz_fx
// ._SwapTile1
// #endasm

    if(checkend < 7 && !CheckPreLines(0)) {
         isPaused = TRUE;

         #asm
         ld de,uncrunch_buffer
         ld hl,_nomorelines
         call uncrunch
         ld hl,uncrunch_buffer
         ld de,$e690+45
         call _put_tile_14x32
         #endasm

         wait(600);

         do {
             reset_grid();
         }
         while (!CheckPreLines(0));

         draw_grid();

         isPaused = FALSE;

    }

    print_cursor();
}

void cambiante(void)
{
    uchar *tile,*tile2;

    frameno++;

//     if(frameno & 0x3)
//     {
//             #asm
//             call _clear_cursor
//             call _print_cursor
//             #endasm
//     }
//     else{

    // Check random tile, Cambiante
    for(n = 0, tile = grid; n < DIMGRID; n++)
    {
        if(n == ty)
        {
            #asm
            call _clear_cursor
            #endasm
        }

        for(m = 0; m < DIMGRID; m++, tile++)
        {
            if(*tile > 11 && ((frameno & 0x3) == 0))
            {
                if(*tile & 0x20)
                {
                    *tile = ((uchar)rand() % 7) | 0x20;
                }
                else
                {
                    *tile = (*tile & 0x1c) + ((frameno & 0x0c) >> 2);
                }

                tile2 = tiles[*tile & 0x1f];
                vram_addr(m*12 + 4, n*24 + 15);
                fast_put_tile(tile2);
            }
        }

        if(n == ty)
        {
            #asm
            call _print_cursor
            #endasm
        }
    }
//    }


}

void SpecialTile(uchar x, uchar y)
{
// 8  = pesa_0
// 12 = reloj_0
// 16 = swap_0
// 20 = rewind_0
// 24 = mas100_0
// 28 = menos100_0

    if((grid[y * DIMGRID + x] & 0x1c) == 20)
    {
        wyz_fx(FX_SPIR);
    }
    else
    {
        wyz_fx(FX_ITEM);
    }

    #asm
    call _clear_cursor
    #endasm
    switch(grid[y * DIMGRID + x] & 0x1c)
    {
        case 8:     // Pesa
            while(y < DIMGRID - 1)
            {
                done = grid[(y+1) * DIMGRID + x];
                if( done < 8){
                    if(tilesD[done])
                    {
                        tilesD[done]--;
                    }
                }

                grid[++y * DIMGRID + x] |= 0x80;
                score++;
                show_score();
                Gravedad();
            }
            score++;
            break;

        case 12:     // Reloj
            #asm
            ld de,uncrunch_buffer
            ld hl,_timex
            call uncrunch
            ld hl,uncrunch_buffer
            call printindic

            ld a,5
            call printindicnumber

            ld b,3
            call settimeindic
            #endasm

            time += 5;
            if (time > 80)
            {
                time = 80;
            }
            break;

        case 16:     // Cruz
            #asm
            ld de,uncrunch_buffer
            ld hl,_inverted
            call uncrunch
            ld hl,uncrunch_buffer
            call printindic

            ld b,5
            call settimeindic
            #endasm

            if(time > 4)
            {
                timeR = time - 5;
            }
            else
            {
                timeR = 0;
            }

            score += 5;
            break;

        case 20:    // Espiral

            do
            {
                reset_grid();
            }
            while (!CheckPreLines(0));

            draw_grid();
            score += 5;

            if(time > 1)
            {
                time -= 2;
            }
            else
            {
                time = 0;
            }

            #asm
            call _print_cursor
            #endasm
            return;

        case 24:    // + 100
            score += 10;
            break;

        case 28:    // - 100
            if(score > 10)
            {
                score -= 10;
            }
            else
            {
                score = 0;
            }
            break;

        default: // Cambiante
            done = grid[y * DIMGRID + x] & 0x07;
            for (n = 0; n < GRIDSIZE; n++)
            {
                if(grid[n] == done)
                {
                    grid[n] |= 0x80;
                    score++;
                }
            }

            cambiante_flag = FALSE;
            break;
    }
    show_score();

    grid[y * DIMGRID + x] |= 0x80;
    explosion(FALSE);
    Gravedad();

    #asm
    call _print_cursor
    #endasm

    SwapTile(0,0,0);
}

void ClearIndic(void){
#asm
    ld de,uncrunch_buffer
    ld hl,_levelx
    call uncrunch
    ld hl,uncrunch_buffer
    ld de,$e690+45
    call _put_tile_14x32

	ld a,(_objetivo)
	call _uchar2dec0
	ld de,$e6cc + 51
	ld hl,_uchar2dec_str
	call _put_string0

	ld a,(_objetivo)
    dec a
    dec a
    call _uchar2dec0

	ld a,(_uchar2dec_str)
	ld hl,_level_seq_number
	ld (hl),a
	ld hl,_level_seq_number0
	ld (hl),a

	ld a,(_uchar2dec_str+1)
	ld hl,_level_seq_number0+1
	ld (hl),a

 	ld de,$e744 + 51
 	ld hl,_uchar2dec_str
 	call _put_string0

	ld hl,_timeindic
	ld (hl),0
#endasm
}

void Interlude(uchar level)
{

    // set all inks to black
    set_pal(pal_black);

    uncrunch(interludio_img[level]);

    wyz_load(level); // requires ordering of songs

    #asm
    ;; set MODE 1
    ld bc,$7f8c+1
    out (c),c

    ;; set palette
    ld hl,_pal_inter
    ld bc,$7f04
    call _set_pal_loop
    #endasm

    print_text_interludio(interludio_text[level]);

    done = FALSE;
    while(!done){
        get_keystate();
        if(KEY_SPACE || JOY_FIRE){
            done = TRUE;
        }
    }

    if(level==5){
        tileset = 0;
        #asm
        call _theendseq
        #endasm
    }

    wyz_pause();
    wyz_reset();

    fade_out_mode1();

    if(level < 5){
        // recover board
        uncrunch(board);
    }

}

void InitTileSet(void)
{
    #asm
    ld bc,_tilesets
    ld a,(_tileset)
    add a
    ld h,0
    ld l,a
    add hl,bc
    ld a,(hl)
    inc hl
    ld h,(hl)
    ld l,a
    ld de,_tile1
    call uncrunch
    #endasm
}

uchar StartLevel(void)
{
    uchar level;

    wyz_pause();
    wyz_reset();

    objetivo++;
    level = objetivo - 3;
    if(game_mode == 0){
        if((level % 5) == 0){
            if(level !=0){
                explosion(TRUE);

                if(level < 25){
                    if(locker <= ++tileset)
                    {
                        locker = tileset;
                    }
                    InitTileSet();
                }
            }

            Interlude(level / 5);
        }

        if(level/5 == 5){
            return 0;
        }
    }

    frameno =0;

    actx = 36; // (36 = 3 x 12)
    acty = 72; // (72 = 3 x 24),

    tx = 3;
    ty = 3;

    objx = tx;
    objy = ty;

    swaping = FALSE;
    fireReleased = TRUE;
    s_tattack = FALSE;

 	ticks = 0;
 	swaping = 0;
 	doble = rand() % 7;
 	timeR = 0;
 	cursor_selected = FALSE;

	reset_grid();
	draw_grid();

	#asm

	; Print time
    ld hl,(_time)
	call _uchar2dec
	ld de,$c81e
	ld hl,_uchar2dec_str
	call _put_string0

    ; Print extra tile
    ld a,(_doble)
    rlca
    ld de,_tiles
    ld l,a
    ld h,0
    add hl,de
    ld a,(hl)
    inc hl
    ld h,(hl)
    ld l,a
    ld de,$cbf5
    call _put_tile_6x24

    ; Initialize tilesD, which keeps count of the completed lines
    ld hl, _tilesD
    ld b,7

.loopinit_tilesD
    ld (hl), 0
    inc hl
    djnz loopinit_tilesD

    call _ClearIndic
    
	ld hl,_tilescore
	ld de,$ee90+2
	ex af,af
	ld a,7

.loop_show_tilescore
	ex af,af
    call _put_tile_4x10
    ld bc,-($836)
    ex de,hl
    add hl,bc
    ex de,hl
    ex af,af
    dec a
    jr nz,loop_show_tilescore
    ex af,af

    ld hl,animframe+1
    ld (hl),0

    ;; set MODE 0
    ld bc,$7f8c
    out (c),c
    #endasm

#asm
	; Initiliaze score and hiscore, 7 digits
	ld de,$c801
	ld hl,_score_init
	call _put_string0
	ld de,$c82e
	ld hl,_score_init
	call _put_string0

    ld hl,(_highs)
    ld (_hiscore),hl
	call _uint2dec
	ld de,$c803+45
	ld hl,_uint2dec_str
	call _put_string0
#endasm

    show_score();

    if(level == 0 || ((level%5) == 0 && game_mode == 0)){
        fade_in();
    }

    wait(200);

    anim_3dstring(level_seq);

    wyz_fx(FX_PAUSA);

    wait(300);

    draw_grid();

    wait(100);

    isPaused = FALSE;

	print_cursor();

    wyz_load(MUSIC_INGAME);

    return 1;
}

uchar Pause(void)
{
    isPaused = TRUE;

    wyz_pause();
    wyz_reset();
    wyz_fx(FX_PAUSA);

    draw_paused_grid();

    keyPressed = TRUE;
    while(TRUE){
        get_keystate();
        if(KEY_SPACE || JOY_FIRE){
            if(!keyPressed){
                isPaused = FALSE;
                wyz_start();
                fireReleased = FALSE;
                draw_grid_from_pause();
                return 0;
            }
            keyPressed = TRUE;
        }
        else if(KEY_ESC){
            if(!keyPressed){
                return 1;
            }
            keyPressed = TRUE;
        }
        else {
            keyPressed = FALSE;
        }
    }
}

void GameCicle(void)
{
    //uchar *tile,*tile2;
    //uchar next_level; // TO BE REMOVED
    uchar salir;
    salir = FALSE;
    //next_level = FALSE; // TO BE REMOVED

    while(!salir){
        get_keystate();

        de = KEY_P || KEY_RIGHT || JOY_RIGHT;
        iz = KEY_O || KEY_LEFT || JOY_LEFT;
        ar = KEY_Q || KEY_UP || JOY_UP;
        ab = KEY_A || KEY_DOWN || JOY_DOWN;

        if(timeR){
            swap(&de, &iz);
            swap(&ar, &ab);
        }

        if(de){
            if(tx < (DIMGRID - 1)){
                if(swaping){
                    SwapTile(tx, ty, 1);
                    swaping = FALSE;
//                     #asm
//                     ld hl,set_animframes+1
//                     ld (hl),animframes
//                     #endasm
                    cursor_selected = FALSE;
                }
                else{
                    objx = tx + 1;
                }
            }
        }

        if(iz){
            if(tx > 0){
                if(swaping){
                    SwapTile(tx, ty, -1);
                    swaping = FALSE;
//                     #asm
//                     ld hl,set_animframes+1
//                     ld (hl),animframes
//                     #endasm
                    cursor_selected = FALSE;

                }
                else{
                    objx = tx - 1;
                }
            }
        }

        if(ar){
            if(ty > 0){
                if(swaping){
                    SwapTile(tx, ty, -DIMGRID);
                    swaping = FALSE;
//                     #asm
//                     ld hl,set_animframes+1
//                     ld (hl),animframes
//                     #endasm
                    cursor_selected = FALSE;

                }
                else{
                    objy = ty - 1;
                }
            }
        }

        if(ab){
            if(ty < (DIMGRID - 1)){
                if(swaping){
                    SwapTile(tx, ty, DIMGRID);
                    swaping = FALSE;
//                     #asm
//                     ld hl,set_animframes+1
//                     ld (hl),animframes
//                     #endasm
                    cursor_selected = FALSE;

                }
                else{
                    objy = ty + 1;
                }
            }
        }

        if(KEY_SPACE || JOY_FIRE){
            if(fireReleased && objx == tx && objy == ty){
                fireReleased = FALSE;
                if(grid[tx + (ty*DIMGRID)] > 7)
                {
                    SpecialTile(tx, ty);
                }
                else
                {
                    swaping = !swaping;
                    if(swaping){
                        wyz_fx(FX_GET);
                        cursor_selected = TRUE;
                        //#asm
                        //ld hl,set_animframes+1
                        //ld (hl),animframes_selection
                        //#endasm
                    }
                    else{
                        cursor_selected = FALSE;
                        //#asm
                        //ld hl,set_animframes+1
                        //ld (hl),animframes
                        //#endasm
                    }
                }
            }
        }
        else{
            fireReleased = TRUE;
        }

        //if(KEY_L){
        //    next_level = TRUE;    // TO BE REMOVED
        //}


        if(tx < objx){
            actx += 2;
        }

        if(tx > objx){
            actx -= 2;
        }

        if(ty < objy){
            acty += 4;
        }

        if(ty > objy){
            acty -= 4;
        }

        if((actx % 12) == 0){
            tx = actx / 12;
        }

        if((acty % 24) == 0){
            ty = acty / 24;
        }

        // Waits until INT4 to update the cursor
        update_cursor();


        // Clear info panel
        if(timeindic > time){
            ClearIndic();
        }

        if(timeR > time){
            timeR = 0;
        }

        if(s_tattack == TRUE){
            if(time >= 10){
                #asm
                ld hl,ttempo
                ld (hl),0
                ld hl,tempo
                inc (hl)
                #endasm
                s_tattack = FALSE;
            }
        }
        else{
            if(time < 10){
                wyz_fx(FX_TIMEATTACK);
                #asm
                ld hl,ttempo
                ld (hl),0
                ld hl,tempo
                dec (hl)
                #endasm
                s_tattack = TRUE;
            }
        }

        if(KEY_ESC){
             salir = Pause();
        }

        if(checkend == 7 /*|| next_level*/){  // TO BE REMOVED
            #asm
            call _clear_cursor
            #endasm

            time += 20;
            if(time > 80){
                time = 80;
            }
            isPaused = 1;
            if(StartLevel() == 0){
                goto checkhigh;
            }

            isPaused = 0;
            //next_level = FALSE;  // TO BE REMOVED
        }

        if(!salir && time == 0){
            salir = 1;
        }
	}

    isPaused = 1;

    // Show possible movements, intermitent

    wyz_pause();
    wyz_reset();

    CheckPreLines(1);
    for(i = 0; i < 3; i++){
        clear_grid();
        wait(75);
        draw_grid();
        wait(75);
    }


    anim_3dstring(gameover_seq);

    wyz_fx(FX_PAUSA);

    wait(500);

    fade_out();

    if(game_mode == 0){
        Interlude(6);

checkhigh:
        #asm
; Check if score > highscore[0..4]
        ld b,5
        xor a
        ld ix,_highs
        ld de,(_score)

.checkhighloop
        ld l,(ix+0)
        ld h,(ix+1)
        sbc hl,de
        jr c,newhigh
        inc ix
        inc ix
        djnz checkhighloop       ; Loop over all highscores
        jp nonewhigh             ; If we reach this point no new highscore

;-------------------------------------------------------------------------
; ix holds position in highscore list where to insert the score
.newhigh
        call _wyz_start

        push bc                  ; Save position in list
        call _cls

        ;; Print high score screen
        ;; set palette
        ld hl,_pal_inter
        ld bc,$7f04
        call _set_pal_loop

        ld de,$fa94+8    ; vram_addr(32,102)
        ld hl,_hiscore_text
        call _put_string

        ld de,$db48+16    ; vram_addr(32,112)
        ld hl,_hiscore_text2
        call _put_string

        ld de,$f3c0+28    ; vram_addr(32,122)
        ld hl,_hiscore_text3
        ld (high_vram),de
        ld (high_dec),hl

        ; Print ???
        ld a,42
        ld (hl),a
        inc hl
        ld (hl),a
        inc hl
        ld (hl),a
        ld hl,(high_dec)
        call _put_string

//         ld a,42
//         call wrchar
//         ld a,42
//         call wrchar
//         ld a,42
//         call wrchar

.high_loop
        ld hl,30
        call _wait

        ld de,(high_vram)
        ld hl,(high_dec)
        ld a,(hl)
        call wrchar

        call _get_keystate

        ld a,(_keystates+5)         ; SPace
        bit 7,a
        jr nz,high_loop_1

        ld a,(_keystates+9)         ; Joy fire
        and $30
        jr nz,high_loop_1

        ld a,(_keystates+8)         ; Q
        bit 3,a
        jr nz,high_increase

        ld a,(_keystates)           ; Cursor up
        bit 0,a
        jr nz,high_increase

        ld a,(_keystates+9)         ; Joy up
        bit 0,a
        jr nz,high_increase

        ld a,(_keystates+8)         ; A
        bit 5,a
        jr nz,high_decrease

        ld a,(_keystates)          ; Cursor down
        bit 2,a
        jr nz,high_decrease

        ld a,(_keystates+9)         ; Joy down
        bit 1,a
        jr nz,high_decrease

        jr high_loop

.high_loop_1
        call _get_keystate
        ld a,(_keystates+5)
        bit 7,a
        jr nz,high_loop_1

        ld a,(_keystates+9)
        and $30
        jr nz,high_loop_1

        ld hl,(high_vram)
        inc hl
        inc hl
        ld (high_vram),hl
        ld hl,(high_dec)
        inc hl
        ld (high_dec),hl
        or a
        ld de,_hiscore_text3+3
        sbc hl,de
        jr z,end_high               ; Check end high score text entering
        jr high_loop


.high_increase
        ld hl,(high_dec)
        ld a,(hl)
        inc a
        cp 45
        jr nz,high_increase_0
        xor a

.high_increase_0
        ld (hl),a
        jp high_loop

.high_decrease
        ld hl,(high_dec)
        ld a,(hl)
        dec a
        cp 255
        jr nz,high_increase_0
        ld a,44
        jr high_increase_0

;--------------------------------------------------------
.end_high
        ; Print high score in menu first
        ld a,1
        ld (_menu_state),a
        dec a
        ld (_cur_menu_state),a

        ; Copy the highscores below score down
        ld (high_vram),ix
        pop bc
        dec b
        ld a,b
        jr z,endcopyhighs

        ld b,0
        ld c,a
        sla c
        ld hl,(high_vram)
        ld de,buffer
        ldir

        ld hl,buffer
        ld de,(high_vram)
        inc de
        inc de
        ld c,a
        sla c
        ldir

.endcopyhighs
        ; Insert score in highscore table
        ld hl,(high_vram)
        ld de,(_score)
        ld (hl),e
        inc hl
        ld (hl),d

; copy highscore strings down
        add a,a
        add a,a
        ld e,a
        ld d,0
        ld hl,_highs-4
        sbc hl,de
        or a
        push hl             ; hl points to first string to move down
        jr z,endcopyhighs0

        ld c,a
        ld de,buffer
        ldir

        ld hl,buffer
        pop de
        push de
        inc de
        inc de
        inc de
        inc de
        ld c,a
        ldir

.endcopyhighs0
        pop de
        ld hl,_hiscore_text3
        ld c,3
        ldir

        call _fade_out_mode1;
        jr exithighs


.nonewhigh
        ; normal menu start, showing options
        ld a,1
        ld (_cur_menu_state),a
        dec a
        ld (_menu_state),a

.exithighs
        call _wyz_pause;
        call _wyz_reset;

        #endasm

    }

gamefinished:
    set_ISR((void *)my_ISR);
}

#asm

.high_dec
    defw 0

.high_vram
    defw 0

#endasm

void InitGame(void)
{
    set_pal(pal_black);

    time = 80;
    score = 0;
 	objetivo = 2;

    if(game_mode == 0){
      tileset = 0;
    }

    InitTileSet();

    uncrunch(board);

    StartLevel();
}


void main(void)
{
    // At this point interrupts are disabled
    #asm
    xor a
    ld (_game_mode),a
    ld (_tileset),a
    ld (_locker),a
    ld (_menu_state),a
    ld (_cur_menu_state),a
    
    ld bc,$7f10                         ; set color border
    out (c),c
    ld a,$54
    out (c),a

    #endasm


    while(TRUE){
        #asm
        ;; set resolution to 120x264
        ;; set width of screen in characters
        ld bc,$bc01
        out (c),c
        ld bc,$bd00+30
        out (c),c

        ;; set horizontal position of screen so that it is centered
        ld bc,$bc02
        out (c),c
        ld bc,$bd00+41
        out (c),c

        ;; set height of screen (vertical displayed) in CRTC characters
        ld bc,$bc06
        out (c),c
        ld bc,$bd00+33
        out (c),c

        ;; set VSYNC
        ld bc,$bc07
        out (c),c
        ld bc,$bd00+35
        out (c),c
        #endasm

        timer_state = 500;

        set_pal(pal_black);
        //disp_off();

        #asm
        ;; set MODE 1
        ld bc,$7f8c+1
        out (c),c

        ld hl,_menu
        call _uncrunch

        ld hl,_hand
        ld de,uncrunch_buffer
        call uncrunch

        call _print_menu

        ld de,$e654+16/2    ; vram_addr(16,220)
        ld hl,_copyright_str
        call _put_string

        ld hl,char_pointer_scroll
        ld (hl),0

        ld hl,tabint_menu
        ld (isr_vector_table+1),hl
        #endasm

        wyz_load(MUSIC_MENU);

	    //set_ISR((void *)my_ISR_menu);

        #asm
        ld a,(_menu_state)
        or a
        ld hl,tabint_menu
        jr z,domenu
        ld hl,tabint_highscore

.domenu
        ld (isr_vector_table+1),hl
        ld hl,_my_ISR_menu
        call _set_ISR
#endasm

	    //disp_on();

        // Menu
        keyPressed = TRUE;
        while(TRUE){
            #asm
//             if(cur_menu_state != menu_state)
//             {
//                 clear_menu();
//                 cur_menu_state = menu_state;
//             }
._loop_menu
            ld a,(_menu_state)
            ld c,a
            ld a,(_cur_menu_state)
            xor c
            jr z, _no_clear_menu
            ld a,c
            ld (_cur_menu_state),a
            ex af,af
            call _clear_menu
            ex af,af
            or a
            jr nz, _do_print_highscore
            call _print_menu
            jr _no_clear_menu
._do_print_highscore
            call _print_highscore

._no_clear_menu
            #endasm

            // get_keystate is called in the ISR
                if((KEY_Q || KEY_UP || JOY_UP) && menu_state == 0){
                    if(!keyPressed){
                        timer_state = 500;
                        if(menu_pointer_pos>0){
                            wyz_fx(FX_PAUSA);
                            menu_pointer_offset = 0xff;
                        }
                    }
                    keyPressed = TRUE;
		        }
		        else if((KEY_A || KEY_DOWN || JOY_DOWN) && menu_state == 0){
                    if(!keyPressed){
                        timer_state = 500;
                        if(menu_pointer_pos < 1 || (menu_pointer_pos < 2 && game_mode != 0)){
                            wyz_fx(FX_PAUSA);
                            menu_pointer_offset = 1;
                        }
                    }
                    keyPressed = TRUE;
		        }
                else if(KEY_SPACE || JOY_FIRE){
                    if(menu_state == 0){
                    if(!keyPressed){
                        // START
                        timer_state = 500;
                        if(menu_pointer_pos == 0){
                            wyz_fx(FX_COMBO);
                            // blinking
                            #asm
                            ld b,5

                            .loop_start
                            push bc
                            ld de,$e2d0+32/2    ; vram_addr(32,100);
                            ld hl,_tile_blank_str
                            call _put_string
                            ld hl,15
                            call _wait
                            ld de,$e2d0+32/2    ; vram_addr(32,100);
                            ld hl,_start_str
                            call _put_string
                            ld hl,15
                            call _wait
                            pop bc
                            djnz loop_start
                            #endasm
                            break;
                        }
                        else if(menu_pointer_pos == 1){
                            wyz_fx(FX_PAUSA);
                            if(++game_mode > 2)
                            {
                                game_mode = 0;
                            }
                            if(game_mode > 1 && locker == 0)
                            {
                                game_mode = 0;
                            }
                        }
                        else{
                            wyz_fx(FX_PAUSA);
                            if(++tileset > locker || tileset > 4){
                                tileset = 0;
                            }
                        }
                    }
                    }
                    else
                    {
                        timer_state = 1;
                    }
                    keyPressed = TRUE;
                }
                else{
                    keyPressed = FALSE;
                }

                #asm
                ld a,(_menu_state)
                or a
                jp nz, _loop_menu

                ld a,(_game_mode)
                add a
                add a
                add a
                ld d,0
                ld e,a
                ld hl,_modes_str
                add hl,de
                ld de,$e348+56/2
                call _put_string
                ld de,$dbc0+32/2    ; vram_addr(32,132)
                #endasm
                if(game_mode > 0){
                    put_string(tile_str);
                    #asm
                    ld a,(_tileset)
                    add a
                    add a
                    add a
                    ld d,0
                    ld e,a
                    ld hl,_tiles_str
                    add hl,de
                    ld de,$dbc0+56/2
                    call _put_string
                    #endasm
                }
                else{
                    put_string(tile_blank_str);
                }
        }

        wyz_pause();
        wyz_reset();

        isPaused = TRUE;

        #asm
        ld hl,tabint_game
        ld (isr_vector_table+1),hl
        #endasm
        set_ISR((void *)my_ISR_menu);

        #asm
        ;; set VSYNC
        ld bc,$bc07
        out (c),c
        ld bc,$bd00+34
        out (c),c
        #endasm

 	    InitGame();
        GameCicle();
    }
}

#ifndef _LIB_H
#define _LIB_H

#define EI asm("ei");
#define DI asm("di");
#define TRUE 1
#define FALSE 0

typedef unsigned char uchar;
typedef unsigned int uint;

extern uchar uint2dec_str[];
extern uchar uchar2dec_str[];

extern void __FASTCALL__ set_pal(uchar *pal);
extern void __FASTCALL__ set_border(uchar color);
extern void __CALLEE__   cls(void);
extern void __CALLEE__   vsync(void);
//extern void __CALLEE__   disp_on(void);
//extern void __CALLEE__   disp_off(void);
extern void __CALLEE__   vram_addr(uint x, uint y);
extern void __FASTCALL__ put_tile_6x24(uchar *tile);
extern void __FASTCALL__ put_tile_6x13(uchar *tile);
extern void __FASTCALL__ put_tile_14x32(uchar *tile);
extern void __FASTCALL__ put_tile_4x10(uchar *tile);
extern void __FASTCALL__ put_tile_4x14(uchar *tile);
extern void __FASTCALL__ put_tile_4x16(uchar *tile);
extern void __FASTCALL__ fast_put_tile(uchar *tile);
extern void __FASTCALL__ fast_put_half_tile(uchar *tile);
extern void __CALLEE__   get_keystate(void);
extern uint __CALLEE__   rand(void);
extern void __CALLEE__   swap(uchar *p1, uchar *p2);
extern void __CALLEE__   draw_paused_grid(void);
extern void __CALLEE__   draw_grid_from_pause(void);
extern void __CALLEE__   draw_grid(void);
extern void __CALLEE__   clear_grid(void);
extern void __FASTCALL__ put_string(uchar *string);
extern void __FASTCALL__ uncrunch(uchar *src);
extern void __FASTCALL__ set_ISR(void *isr);
extern void __FASTCALL__ wait(uint timer);
extern void __FASTCALL__ uint2dec(uint a);
extern void __FASTCALL__ uchar2dec(uchar a);
extern void __CALLEE__   fade_in(void);
extern void __CALLEE__   fade_out(void);
extern void __CALLEE__   fade_out_mode1(void);
extern void __FASTCALL__ anim_3dstring(uchar *string);
extern void __CALLEE__   clear_menu(void);
extern void __FASTCALL__ print_text_interludio(uchar *string);

extern uchar keystates[10];

#asm
.exo_mapbasebits
    defs	156	                       ;tables for bits, baseL, baseH

;; HL = tile
;; DE = vram_addr
._put_tile_14x32
    ld a,14
    ld iyh,32
    jp _put_tile_0

._put_tile_6x24
    ld iyh,24
    jp _put_tile_6

._put_tile_4x14
    ld iyh,14
    ld a,4
    jp _put_tile_0

._put_tile_4x10
    ld iyh,10
    ld a,4
    jp _put_tile_0


//    defs 2

// ; Fill the rest of the 256 byte page with some code (100 bytes)
// ;-------------------------------------------------------------------------------
// ._vsync
//     ld b,$f5
//
// .vsync1
//     in a,(c)
//     rra
//     jr c,vsync1
//
// .vsync2
//     in a,(c)
//     rra
//     jr nc,vsync2
//     ret
//

;-------------------------------------------------------------------------------
._cls
    ld hl,$c000
    ld de,$c001
    ld bc,$3fff
    ld (hl),0
    ldir

    ret

    defs 7
;-------------------------------------------------------------------------------
; Enable display output
// ._disp_on
//     ld bc,$bc08
//     out (c),c
//     inc b
//     xor a
//     out (c),a
// 
//     ret

;-------------------------------------------------------------------------------
; Blank the display
// ._disp_off
//     ld bc,$bc08
//     out (c),c
//     inc b
//     ld c,$30
//     out (c),c
//     ret

//;-------------------------------------------------------------------------------
// ._cls
//     ld hl,$c000
//     ld de,$c001
//     ld bc,$3fff
//     ld (hl),l
//     ldir
//
//     ret
;-------------------------------------------------------------------------------
._set_pal
    ld bc,$7f10        ; #7fxx GATE ARRAY IO port

._set_pal_loop
    ld a,(hl)          ; HL = pointer to palette
    inc hl
    dec c
    out (c),c          ; Select PEN
    out (c),a          ; Select color for selected pen
    jr nz,_set_pal_loop
    ret

;-------------------------------------------------------------------------------
._set_border
    ld bc,$7f10
    out (c),c
    out (c),l

    ret

;-------------------------------------------------------------------------------
;; DE = output address
._vram_addr
    pop af
    pop hl
    pop de
    push af

    add hl,hl
    ld bc,_addrtbl
    add hl,bc
    ld a,(hl)
    inc hl
    ld h,(hl)
    ld l,a
    srl e
    add hl,de
    ex de,hl
    ret

;-------------------------------------------------------------------------------

._keystates
defs 10

;-------------------------------------------------------------------------------

; it at a 256byte border so we can use inc l instead of inc hl
.MASKTBL
defb $ff,$aa,$55,$00,$aa,$aa,$00,$00
defb $55,$00,$55,$00,$00,$00,$00,$00
defb $aa,$aa,$00,$00,$aa,$aa,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $55,$00,$55,$00,$00,$00,$00,$00
defb $55,$00,$55,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $aa,$aa,$00,$00,$aa,$aa,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $aa,$aa,$00,$00,$aa,$aa,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $55,$00,$55,$00,$00,$00,$00,$00
defb $55,$00,$55,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $55,$00,$55,$00,$00,$00,$00,$00
defb $55,$00,$55,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00
defb $00,$00,$00,$00,$00,$00,$00,$00

.SELTBL
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 64,5,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,69,0,0,84,1,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,2,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 192,0,74,15,0,0,0,0
defb 0,0,0,0,0,0,0,0
defb 0,0,0,79,212,0,94,11
defb 0,0,0,0,0,0,0,0
defb 0,0,0,143,0,0,0,0
defb 232,173,0,7,0,0,0,0
defb 0,0,0,207,0,0,0,139
defb 0,0,0,71,252,169,86,3


; Buffer for cursor, it at a 256byte border so we can use inc l instead of inc hl
.buffer
defs 112

; ADDRTBL which is used in vram_addr
;-------------------------------------------------------------------------------
._addrtbl
defb   0,192,  0,200,  0,208,  0,216,  0,224,  0,232,  0,240,  0,248
defb  60,192, 60,200, 60,208, 60,216, 60,224, 60,232, 60,240, 60,248
defb 120,192,120,200,120,208,120,216,120,224,120,232,120,240,120,248
defb 180,192,180,200,180,208,180,216,180,224,180,232,180,240,180,248
defb 240,192,240,200,240,208,240,216,240,224,240,232,240,240,240,248
defb  44,193, 44,201, 44,209, 44,217, 44,225, 44,233, 44,241, 44,249
defb 104,193,104,201,104,209,104,217,104,225,104,233,104,241,104,249
defb 164,193,164,201,164,209,164,217,164,225,164,233,164,241,164,249
defb 224,193,224,201,224,209,224,217,224,225,224,233,224,241,224,249
defb  28,194, 28,202, 28,210, 28,218, 28,226, 28,234, 28,242, 28,250
defb  88,194, 88,202, 88,210, 88,218, 88,226, 88,234, 88,242, 88,250
defb 148,194,148,202,148,210,148,218,148,226,148,234,148,242,148,250
defb 208,194,208,202,208,210,208,218,208,226,208,234,208,242,208,250
defb  12,195, 12,203, 12,211, 12,219, 12,227, 12,235, 12,243, 12,251
defb  72,195, 72,203, 72,211, 72,219, 72,227, 72,235, 72,243, 72,251
defb 132,195,132,203,132,211,132,219,132,227,132,235,132,243,132,251
defb 192,195,192,203,192,211,192,219,192,227,192,235,192,243,192,251
defb 252,195,252,203,252,211,252,219,252,227,252,235,252,243,252,251
defb  56,196, 56,204, 56,212, 56,220, 56,228, 56,236, 56,244, 56,252
defb 116,196,116,204,116,212,116,220,116,228,116,236,116,244,116,252
defb 176,196,176,204,176,212,176,220,176,228,176,236,176,244,176,252
defb 236,196,236,204,236,212,236,220,236,228,236,236,236,244,236,252
defb  40,197, 40,205, 40,213, 40,221, 40,229, 40,237, 40,245, 40,253
defb 100,197,100,205,100,213,100,221,100,229,100,237,100,245,100,253
defb 160,197,160,205,160,213,160,221,160,229,160,237,160,245,160,253
defb 220,197,220,205,220,213,220,221,220,229,220,237,220,245,220,253
defb  24,198, 24,206, 24,214, 24,222, 24,230, 24,238, 24,246, 24,254
defb  84,198, 84,206, 84,214, 84,222, 84,230, 84,238, 84,246, 84,254
defb 144,198,144,206,144,214,144,222,144,230,144,238,144,246,144,254
defb 204,198,204,206,204,214,204,222,204,230,204,238,204,246,204,254
defb   8,199,  8,207,  8,215,  8,223,  8,231,  8,239,  8,247,  8,255
defb  68,199, 68,207, 68,215, 68,223, 68,231, 68,239, 68,247, 68,255
defb 128,199,128,207,128,215,128,223,128,231,128,239,128,247,128,255


;-------------------------------------------------------------------------------
._vram_addr_save
defs 2

;-------------------------------------------------------------------------------
;; HL = tile
;; DE = vram_addr
// ._put_tile_14x32
//     ld a,14
//     ld iyh,32
//     jp _put_tile_0
//
// ._put_tile_6x24
//     ld iyh,24
//     ld a,6
//     jp _put_tile_0
//
// ._put_tile_6x13
//     ld iyh,13
//     ld a,6
//     jp _put_tile_0
//
// ._put_tile_4x10
//     ld iyh,10
//     ld a,4
//     jp _put_tile_0

._put_tile_6x13
    ld iyh,13
._put_tile_6
    ld a,6

;-------------------------------------------------------------------------------
._put_tile_0
    ld (_put_tile_loop+1),a

._put_tile_loop
    ld a,0
    push de

._put_tile_loop_0
    ldi
    dec a
    jp nz,_put_tile_loop_0
    pop de
    dec iyh
    ret z

._put_tile_loop_fix
    ld a,d
    add 8
    ld d,a
    jp nc,_put_tile_loop

    ex de,hl
    ld bc,$c03c
    add hl,bc
    ex de,hl
    jp _put_tile_loop

._fast_put_half_tile
    ld a,12
    jp _fast_put_tile_loop

._fast_put_tile
    ld a,24

._fast_put_tile_loop
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi

    dec a
    ret z

    ex de,hl
    ld bc,$800-6
    add hl,bc
    ex de,hl
    jp nc,_fast_put_tile_loop

    ex de,hl
    ld bc,$c03c
    add hl,bc
    ex de,hl
    jp _fast_put_tile_loop

;-------------------------------------------------------------------------------
;; HL = tile
;; DE = vram_addr
._put_tile_transparency
    ex de,hl                  ; de = tile / hl = vram_addr
    ld a,14                   ; height

.loop_tile_transparency
    ld b,MASKTBL/256
    ex af,af

    ld iyh,4                  ; width
.loop_tile_transparency_0
    ld a,(de)                 ; tile
    inc de
    ld c,a
    ld a,(bc)                 ; mask
    and (hl)
    or c
    ld (hl),a                 ; vram_addr
    inc hl

    dec iyh
    jr nz,loop_tile_transparency_0

    ex af,af
    dec a
    ret z

	ld bc,$7fc
	add hl,bc
    jp nc,loop_tile_transparency

    ld bc,$c03c
    add hl,bc
    jp loop_tile_transparency

;-------------------------------------------------------------------------------
._get_keystate
    di
    ld de,_keystates

    ld bc,$f40e
    ld h,b
    out (c),c
    ld bc,$f6c0
    ld l,b
    out (c),c
    ld c,0
    out (c),c
    inc b
    ld a,$92
    out (c),a
    push bc
    set 6,c
.scan_line
    ld b,l
    out (c),c
    ld b,h
    in a,(c)
    cpl
    ld (de),a
    inc c
    inc de
    ld a,c
    cp $4a
    jr nz,scan_line
    pop bc
    ld a,$82
    out (c),a
    dec b
    out (c),c
    ei
    ret

;-------------------------------------------------------------------------------
; Uses a 16-bit RAM variable called RandomNumberGeneratorWord
; Returns an 8-bit pseudo-random number in hl
.RandomNumberGeneratorWord
defs 2

._rand
    ld hl,(RandomNumberGeneratorWord)
    ld a,h         ; get high byte
    rrca           ; rotate right by 2
    rrca
    xor h          ; xor with original
    rrca           ; rotate right by 1
    xor l          ; xor with low byte
    rrca           ; rotate right by 4
    rrca
    rrca
    rrca
    xor l          ; xor again
    rra            ; rotate right by 1 through carry
    adc hl,hl      ; add RandomNumberGeneratorWord to itself
    jr nz,_rand1
    ld hl,$733c    ; if last xor resulted in zero then re-seed random number generator
._rand1
	ld a,r         ; r = refresh register = semi-random number
    xor l          ; xor with l which is fairly random
    ld (RandomNumberGeneratorWord),hl
    ld h,0
    ld l,a
	ret

// ;In, nothing
// ;Out, A with a random number
// ;Author, Ricardo Bittencourt aka RicBit (BrMSX, Tetrinet and several other projects)
// ; choose a random number in the set [0,255] with uniform distribution
// ._rand
//     ld hl,(RandomNumberGeneratorWord)
//     add hl,hl
//     sbc a,a
//     and $83
//     xor l
//     ld l,a
//     ld (RandomNumberGeneratorWord),hl
//     ret



;-------------------------------------------------------------------------------
._clear_grid
    ld hl,uncrunch_buffer
    ld (hl),0x80
    ld d,h
    ld e,l
    ld bc,63
    inc de
    ldir
    ld bc,uncrunch_buffer
    jr _draw_grid0

;-------------------------------------------------------------------------------
._draw_paused_grid_wait
    ld hl,12
    jp _wait

._draw_paused_grid
	call _draw_paused_grid_wait
    ld de,$f83e
	call _print_paused_grid_row
 	ld de,$fd2a
	call _print_paused_grid_row

	call _draw_paused_grid_wait
    ld de,$f8f2
	call _print_paused_grid_row
 	ld de,$fc76
	call _print_paused_grid_row

	call _draw_paused_grid_wait
    ld de,$f9a6
	call _print_paused_grid_row
 	ld de,$fbc2
	call _print_paused_grid_row

	call _draw_paused_grid_wait
    ld de,$fa5a
	call _print_paused_grid_row
 	ld de,$fb0e

._print_paused_grid_row
    ld iyl,8
._print_paused_grid_row_loop
    push de
    exx
    ld hl,_pausa_tile
    push hl
    exx
    pop hl
    call _fast_put_tile
    pop de
    inc de
    inc de
    inc de
    inc de
    inc de
    inc de
    dec iyl
    jp nz,_print_paused_grid_row_loop
    ret

;-------------------------------------------------------------------------------
._draw_grid
    ld bc,_grid

._draw_grid0
	exx
	ld de,$f83e
	call _print_grid_row
	ld de,$f8f2
	call _print_grid_row
	ld de,$f9a6
	call _print_grid_row
	ld de,$fa5a
	call _print_grid_row
 	ld de,$fb0e
 	call _print_grid_row
 	ld de,$fbc2
 	call _print_grid_row
 	ld de,$fc76
 	call _print_grid_row
 	ld de,$fd2a
    jp _print_grid_row

;-------------------------------------------------------------------------------
._draw_grid_from_pause

	ld bc,_grid+24
	exx
	call _draw_paused_grid_wait
	ld de,$fa5a
	call _print_grid_row
	exx


	ld bc,_grid+32
	exx
	ld de,$fb0e
 	call _print_grid_row
    exx

	ld bc,_grid+16
	exx
	call _draw_paused_grid_wait
    ld de,$f9a6
	call _print_grid_row
	exx

	ld bc,_grid+40
	exx
 	ld de,$fbc2
 	call _print_grid_row
 	exx

	ld bc,_grid+8
	exx
	call _draw_paused_grid_wait
	ld de,$f8f2
	call _print_grid_row
	exx

	ld bc,_grid+48
	exx
 	ld de,$fc76
 	call _print_grid_row
    exx

	ld bc,_grid
	exx
	call _draw_paused_grid_wait
	ld de,$f83e
	call _print_grid_row
	exx

	ld bc,_grid+56
	exx
 	ld de,$fd2a

._print_grid_row
    ld iyl,8
._print_grid_row_loop
    push de
    exx
    ld a,(bc)
    inc bc
    bit 7,a
    jr nz,_print_grid_empty_tile
    and $1f
    rlca
    ld de,_tiles
    ld l,a
    ld h,0
    add hl,de
    ld a,(hl)
    inc hl
    ld h,(hl)
    ld l,a
    jr _print_grid_tile

._print_grid_empty_tile
    ld hl,_explosion4

._print_grid_tile
    push hl
    exx
    pop hl
    call _fast_put_tile
    pop de
    inc de
    inc de
    inc de
    inc de
    inc de
    inc de
    dec iyl
    jp nz,_print_grid_row_loop
    ret

;-------------------------------------------------------------------------------
._swap
	pop af
	pop de
	pop hl
	push af

	ld a,(de)
	ldi
	dec hl
	ld (hl),a

	ret

;-------------------------------------------------------------------------------
; de contains the screen address
._put_string
    ld a,(hl)
    inc hl
    cp $ff
    ret z
    bit 7,a
    jr z,next_char
    and $7f
    jp wrchar

.next_char
    call wrchar
    jr _put_string

.wrchar
    push hl
    push de
    ld l,a
    ld h,0
    add hl,hl
    ld b,h
    ld c,l
    add hl,hl
    add hl,hl
    add hl,hl
    sbc hl,bc
    ld bc,_fonts            ; can be reduced to inc h if the fonts starts at 0100h
    add hl,bc

    ld a,7                  ; character height
.loop_wrchar
    ldi                     ; Two bytes per char width
    ldi
    ex de,hl
    ld bc,$7fe;
    add hl,bc
    jr nc,next_line_wrchar
    ld bc,$c03c
    add hl,bc

.next_line_wrchar
    ex de,hl
    dec a
    jr nz,loop_wrchar

    pop de
    pop hl
    inc de
    inc de

    ret
;-------------------------------------------------------------------------------

._put_string0
    ld a,(hl)
    inc hl
    cp $ff
    ret z
    and $7f
    call wrchar0
    jr _put_string0

.wrchar0
    push hl
    push de
    ld l,a
    ld h,0
    add hl,hl
    add hl,hl
    ld b,h
    ld c,l
    add hl,hl
    add hl,hl
    sbc hl,bc
    ld bc,number_set1            ; can be reduced to inc h if the fonts starts at 0100h
    add hl,bc

    ld a,6                  ; character height
.loop_wrchar0
    ldi                     ; Two bytes per char width
    ldi
    ex de,hl
    ld bc,$7fe;
    add hl,bc
    jr nc,next_line_wrchar0
    ld bc,$c03c
    add hl,bc

.next_line_wrchar0
    ex de,hl
    dec a
    jr nz,loop_wrchar0

    pop de
    pop hl
    inc de
    inc de

    ret

;-------------------------------------------------------------------------------
;## Decompress a pucrunched file.
;## In      : HL = source address
;##           DE = destination address
;## Destroy : AF, BC, DE, HL, AF', BC', DE', HL'
._uncrunch
; Exomizer 2 Z80 decoder
; by Metalbrain
;
; compression algorithm by Magnus Lind
;
; simple version:
;		no literal sequences
;		you MUST define exo_mapbasebits aligned to a 256 boundary
;
; input:
;       hl=compressed data start
;		de=uncompressed destination start

    ld de,$c000

.uncrunch
    ld ixh,128
    ld b,52
    ld iy,exo_mapbasebits
    push de

.exo_initbits
    ld a,b
	sub	4
	and	15
	jr nz,exo_node1
	ld de,1		              ; DE=b2

.exo_node1
    ld c,16

.exo_get4bits
    call exo_getbit
	rl c
	jr nc,exo_get4bits
	ld (iy+0),c	              ; bits[i]=b1

	push hl
	inc	c
	ld hl,0
	scf

.exo_setbit
    adc	hl,hl
	dec	c
	jr nz,exo_setbit
	ld (iy+52),e
	ld (iy+104),d	          ; base[i]=b2
	add	hl,de
	ex de,hl
	inc	iy

	pop	hl
	djnz exo_initbits
	pop	de

.exo_literalcopy
    ldi

.exo_mainloop
    call exo_getbit	          ; literal?
	jr c,exo_literalcopy
	ld c,255

.exo_getindex
    inc	c
	call exo_getbit
	jr nc,exo_getindex
	ld a,c		              ; C=index
	cp 16
	ret	z
	push de
	call exo_getpair
	push bc
	pop	af
	ex af,af		          ; lenght in AF
	ld de,512+48	          ; 1?
	dec	bc
	ld a,b
	or c
	jr z,exo_goforit
	ld de,1024+32
	dec	bc		              ; 2?
	ld a,b
	or c
	jr z,exo_goforit
	ld e,16

.exo_goforit
    call exo_getbits
	ld a,e
	add	a,c
	ld c,a
	call exo_getpair	      ; bc=offset
	pop	de		              ; de=destination
	push hl
	ld h,d
	ld l,e
	sbc	hl,bc		          ; hl=origin
	ex af,af
	push af
	pop	bc		              ; bc=lenght
	ldir
	pop	hl		              ; Keep HL, DE is updated
	jr exo_mainloop	          ; Next!

.exo_getpair
    ld iyl,c
	ld d,(iy+0)
	call exo_getbits
	ld a,c
	add	a,(iy+52)
	ld c,a
	ld a,b
	adc	a,(iy+104)	          ; Always clear C flag
	ld b,a
	ret

.exo_getbits
    ld bc,0		              ; get D bits in BC

.exo_gettingbits
    dec	d
	ret	m
	call exo_getbit
	rl c
	rl b
	jr exo_gettingbits

.exo_getbit
    ld a,ixh		          ; get one bit
	add	a,a
	ld ixh,a
	ret	nz
	ld a,(hl)
	inc	hl
	rla
	ld ixh,a
	ret


;-------------------------------------------------------------------------------
._set_ISR
    ld ($39),hl
    ei
    ret

;-------------------------------------------------------------------------------
._wait
    halt
    dec hl
    ld a,h
    or l
    jr nz,_wait
    ret

;-------------------------------------------------------------------------------
; 16-bit Integer to ASCII (decimal)
; Input: HL = number to convert, DE = location of ASCII string
; Output: ASCII string at (DE)

._uint2dec
    ld de,_uint2dec_str
    ld bc,-10000
	call num1
	ld bc,-1000
	call num1
	ld bc,-100
	call num1
	ld c,-10
	call num1
	ld c,-1

.num1
    ld a,-1

.num2
    inc a
	add hl,bc
	jr c,num2
	sbc hl,bc

	ld (de),a
	inc de
	ret

;-------------------------------------------------------------------------------
; Decimal to ASCII 2 digits only
._uchar2dec
    ld a,l

._uchar2dec0
    ld hl,_uchar2dec_str
    ld bc,$ff00+10

.dtoa2dloop
    inc b                         ; Increase the number of tens
    sub c                         ; Take away one unit of ten from A
    jr nc,dtoa2dloop              ; If A still hasnt gone negative, do another
    add c                         ; Decreased it too much, put it back
    ld (hl),b
    inc hl
    ld (hl),a
    ret

._uint2dec_str
    defs 3
._uchar2dec_str
    defs 2
    defb $ff

;-------------------------------------------------------------------------------
; Fade in
._fade_in
    ld a,$06

.loop_fade_in
    call fade
    dec a
    cp $ff
    jr nz,loop_fade_in
    ret

;-------------------------------------------------------------------------------
; Fade out
._fade_out
    xor a

.loop_fade_out
    call fade
    inc a
    cp 7
    jr nz,loop_fade_out
    ret

;-------------------------------------------------------------------------------
.fade
    push af
    ld b,12

.loop_fade
    ei
    halt
    djnz loop_fade
    
    ex af,af
    call _vsync
    ex af,af

    add a
    add a
    add a
    add a
    ld d,0
    ld e,a
    ld hl,_pal_game
    add hl,de
    call _set_pal
    pop af
    ret

;-------------------------------------------------------------------------------
; Fade out
._fade_out_mode1
    xor a

.loop_fade_out_mode1
    call fade_mode1
    inc a
    cp 6
    jr nz,loop_fade_out_mode1
    ret

;-------------------------------------------------------------------------------
.fade_mode1
    push af
    ld b,12

.loop_fade_mode1
    ei
    halt
    djnz loop_fade_mode1

    ex af,af
    call _vsync
    ex af,af

    add a
    add a
    ld d,0
    ld e,a
    ld hl,_pal_inter
    add hl,de
    ld bc,$7f04
    call _set_pal_loop
    pop af
    ret

;-------------------------------------------------------------------------------
._anim_3dstring
    push hl
    pop ix

._anim_3dstring_0
    ld hl,12
    call _wait
    ld de,$fa5a+7

._anim_3dstring_1
    ld a,(ix+0)
    cp $ff
    ret z

    and $7f
    ld bc,_3dfont
    call get_3dchar

    push de
    call _put_tile_transparency
    pop de

    ld a,(ix+0)
    inc ix
    bit 7,a    ; a[7]=0 => Z=1
    jr nz,_anim_3dstring_0

    inc de
    inc de
    inc de
    inc de

    jr _anim_3dstring_1

.get_3dchar

    push bc
    ld l,a
    ld h,0
    add hl,hl
    add hl,hl
    add hl,hl
    ld b,h
    ld c,l
    add hl,hl
    add hl,hl
    add hl,hl
    sbc hl,bc
    pop bc
    add hl,bc
    ret

;-------------------------------------------------------------------------------
._clear_menu
    ld hl,$ca58+9
    ld d,64
    xor a
._clear_menu_loop
    ld b,38
._clear_menu_scanline
    ld (hl),a
    inc hl
    djnz _clear_menu_scanline

    dec d
    ret z

    ld bc,$800-38
    add hl,bc
    jr nc,_clear_menu_loop

    ld bc,$c03c
    add hl,bc
    jr _clear_menu_loop

;-------------------------------------------------------------------------------
.printindicnumber
    add a
    add a
    add a
    ld l,a
    ld h,0
    add hl,hl
    add hl,hl
    add hl,hl
    ld c,a
    ld b,0
    or a
    sbc hl,bc
    ld de,_3dfont
    add hl,de
    ld de,$f708+50
    jp _put_tile_4x14

.printindic
    ld de,$e690+45
    jp _put_tile_14x32

.settimeindic
    ld hl,_timeindic
    ld a,(_time)
    sub b
    ld (hl),a
    ret

;-------------------------------------------------------------------------------
; hl = text
._print_text_interludio
    ex de,hl
    ld hl,_paused_interludio
    ld (hl),1
    ex de,hl

    ld de,$c384+2
    call _put_string_slow
    ld de,$e3c0+2
    call _put_string_slow
    ld de,$c438+2
    call _put_string_slow
    ld de,$e474+2
    call _put_string_slow
    ld de,$c4ec+2
    call _put_string_slow
    ld de,$e528+2
    call _put_string_slow
    ld de,$c5a0+2
    call _put_string_slow
    ld de,$e5dc+2
    call _put_string_slow
    ld de,$c654+2
    call _put_string_slow
    ld de,$e690+2

._put_string_slow
    exx
    call _get_keystate
    ld de,_keystates
    ld hl,5
    add hl,de
    ld a,(hl)
    and $80
    jr nz,_go_nopause_interludio

    ld hl,9
    add hl,de
    ld a,(hl)
    and $30
    jr nz,_go_nopause_interludio

    ld hl,_paused_interludio
    ld a,(hl)
    or a
    jr z,_go_nopause_interludio

    ld hl,18
    call _wait
    jr _go_pause_interludio

._go_nopause_interludio
    ld hl,_paused_interludio
    ld (hl),0

._go_pause_interludio
    exx

    ld a,(hl)
    cp $ff
    ret z
    inc hl
    bit 7,a
    jr z,_next_char_slow
    and $7f
    call wrchar
    ret

._next_char_slow
    call wrchar
    jr _put_string_slow

._paused_interludio
defb 1

._vsync
    ld b,$f5

.vsync1
    in a,(c)
    rra
    jr nc,vsync1

    ret

._theendseq
    ld a,($bfff)
    or a
    ret nz              ; return if only 64k found

;    call _disp_off
    ld hl,_pal_black
    ld bc,$7f04
    call _set_pal_loop


    call _wyz_pause
    call _wyz_reset

    di
; swapram7
    ld bc,$7fc7
    out (c),c

;    ld hl,$4000
;    ld de,$c000
;    ld bc,$3fff
;    ldir

    ld hl,$4000
    call _uncrunch


; swapram0
    ld bc,$7fc0
    out (c),c

    call _wyz_start

    ei

._clear_keybuffer_loop
    call _get_keystate;

	ld hl,_keystates+5
	ld a,(hl)
	rlca
	jr c,_clear_keybuffer_loop
	ld hl,_keystates+9
	ld a,(hl)
	and	48
	jr nz,_clear_keybuffer_loop

;    call _disp_on
    ld hl,_pal_inter
    ld bc,$7f04
    call _set_pal_loop

    call _set_tile0
._theendseqloop
    call _draw_peli_tiles
    call _vsync

    call _get_keystate;

	ld	hl,_keystates+5
	ld	a,(hl)
	rlca
	ret c
	ld	hl,_keystates+9
	ld	a,(hl)
	and	48
	ret nz
    jr _theendseqloop


._draw_peli_tiles
    ld de,$e83c
    ld ixh,15

._draw_peli_tiles_loop
    ld hl,_tilepeli0
    call _put_tile
    ex de,hl
    ld bc,$614
    add hl,bc
    ex de,hl

._draw_peli_tiles_loop_0
    ld hl,_tilepeli0
    call _put_tile
    ex de,hl
    ld bc,$8f8
    add hl,bc
    ex de,hl

    dec ixh
    jp z,_end_draw_peli_tiles
    jp _draw_peli_tiles_loop

._end_draw_peli_tiles

    ld hl,_frameno
    inc (hl)
    ld a,(hl)

    ld bc,$bc00+5
    out (c),c
    inc b

    cp 3
    jr z,_set_tile1
    cp 6
    jr z,_set_tile2
    cp 9
    jr z,_set_tile3
    cp 12
    jr z,_set_tile0
    ret

._set_tile1
    ld hl,_tilepeli1
    xor a
    jr _set_tile

._set_tile2
    ld hl,_tilepeli2
    ld a,2
    jr _set_tile

._set_tile3
    ld hl,_tilepeli3
    ld a,1
    jr _set_tile

._set_tile0
    ld hl,_frameno
    ld (hl),0
    ld hl,_tilepeli0
    ld a,1

._set_tile
    ld (_draw_peli_tiles_loop+1),hl
    ld (_draw_peli_tiles_loop_0+1),hl
    out (c),a

    ret

;-------------------------------------------------------------------------------
;; HL = tile
;; DE = vram_addr

._put_tile
    ld a,16

    ex de,hl

.loop_tile
    ex de,hl

    ldi
    ldi
    ldi
    ldi

    dec a
    ret z

    ex de,hl
    ld bc,$7fc
    add hl,bc
    jp nc,loop_tile

    ld bc,$c03c
    add hl,bc
    jp loop_tile


#endasm

#define KEY_Q     ( keystates[8] & 0x08 )
#define KEY_A     ( keystates[8] & 0x20 )
#define KEY_P     ( keystates[3] & 0x08 )
#define KEY_O     ( keystates[4] & 0x04 )
#define KEY_SPACE ( keystates[5] & 0x80 )
#define KEY_ESC   ( keystates[8] & 0x04 )
#define JOY_FIRE  ( keystates[9] & 0x30 )
#define JOY_RIGHT ( keystates[9] & 0x08 )
#define JOY_LEFT  ( keystates[9] & 0x04 )
#define JOY_UP    ( keystates[9] & 0x01 )
#define JOY_DOWN  ( keystates[9] & 0x02 )
#define KEY_UP    ( keystates[0] & 0x01 )
#define KEY_DOWN  ( keystates[0] & 0x04 )
#define KEY_RIGHT ( keystates[0] & 0x02 )
#define KEY_LEFT  ( keystates[1] & 0x01 )

// TO BE REMOVED
#define KEY_L     ( keystates[4] & 0x10 )

#endif
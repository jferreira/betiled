org #a000

CAS_IN_OPEN   equ #bc77
CAS_IN_DIRECT equ #bc83
CAS_IN_CLOSE  equ #bc7a

disp_off
;     ld bc,$bc08
;     out (c),c
;     inc b
;     ld c,$30
;     out (c),c

    xor a                               ; determine if 64k or bigger
    ld (#4000),a
    ld bc,#7fc7                         ; swapram7
    out (c),c
    inc a
    ld (#4000),a
    ld bc,#7fc0                         ; go back to normal config
    out (c),c
    ld a,(#4000)
    ld (#bfff),a                        ; if a = 0, the system has 128k

    ld hl,start
    ld de,#00a0
    ld bc,endloader-start+1
    ldir
    jp #00a0

start
    ld b,#05
    ld hl,intro
    call CAS_IN_OPEN
    ld hl,#3000
    call CAS_IN_DIRECT
    call CAS_IN_CLOSE

    call #80a5

; Display intro screen
;     ld b,#06
;     ld hl,screen
;     ld de,#c000
;     call CAS_IN_OPEN
;     ld hl,#c000
;     call CAS_IN_DIRECT
;     call CAS_IN_CLOSE

; disp_on
;     ld bc,$bc08
;     out (c),c
;     inc b
;     xor a
;     out (c),a

; Load first bank into #4000
    ld b,11
    ld hl,bank0
    call CAS_IN_OPEN
    ld hl,#01f9
    call CAS_IN_DIRECT
    call CAS_IN_CLOSE

; Move the data
;    ld hl,#8000
;    ld de,#01f9
;    ld bc,#4000
;    ldir

; Load second bank into #4000
    ld b,11
    ld hl,bank1
    call CAS_IN_OPEN
    ld hl,#41f9
    call CAS_IN_DIRECT
    call CAS_IN_CLOSE

; Move the data
;    ld hl,#8000
;    ld de,#41f9
;    ld bc,#4000
;    ldir

; swapram7
;     ld bc,#7fc7
;     out (c),c
;     ld b,#06
;     ld hl,bank3
;     ld de,#c000
;     call CAS_IN_OPEN
;     ld hl,#4000
;     call CAS_IN_DIRECT
;     call CAS_IN_CLOSE
; swapram0
;     ld bc,#7fc0
;     out (c),c

; disp_off
;    ld bc,$bc08
;    out (c),c
;    inc b
;    ld c,$30
;    out (c),c

; Black out screen using firmaware
    ld a,0
loop:
    push af
    ld bc,#0000
    call #bc32
    pop af
    inc a
    cp 16
    jr nz,loop

; Load third bank into screen memory
    ld b,11
    ld hl,bank2
    call CAS_IN_OPEN
    ld hl,#c000
    call CAS_IN_DIRECT
    call CAS_IN_CLOSE

; Move the data
    di
    ld hl,#c000
    ld de,#81f9
    ld bc,#4000
    ldir

; Start execution
    jp #01f9


intro
defm "intro"

;screen
;defm "screen"

bank0
defm "betiled.bi0"

bank1
defm "betiled.bi1"

bank2
defm "betiled.bi2"

;bank3
;defm "theend"

endloader

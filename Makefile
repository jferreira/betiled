ZCC_DIR:=/mnt/ext/Homebrew/tools/z88dk
IDSK_DIR:=/mnt/ext/Homebrew/machines/cpc/toolbox/idsk
PASMO_DIR:=/mnt/ext/Homebrew/toolbox/pasmo-0.5.3

.EXPORT_ALL_VARIABLES:

PATH := $(PATH):${ZCC_DIR}/bin:${PASMO_DIR}:${IDSK_DIR}/bin
ZCCCFG := ${ZCC_DIR}/lib/config

all: betiled.dsk

betiled.dsk: loader.bin intro.bin betiled.bas betiled.bi0 betiled.bi1 betiled.bi2
	@echo Creating dsk...
	@rm -f $@
	@iDSK -n $@
	$(foreach FILE,$^, iDSK $@ -i $(FILE);)

betiled.bi0 betiled.bi1 betiled.bi2 : betiled.bin
	@split -b 16384 -d -a 1 betiled.bin betiled.bi


betiled.bin: main.c lib.h sprite.h tiles.h wyz.h text.h crt0.s
	@zcc +cpc -vn -Wall -O3 -crt0=crt0.s $< -o betiled.bin -create-app -m -s

loader.bin: loader.asm
	@pasmo --amsdos --alocal loader.asm loader.bin

intro.bin: intro.asm
	@pasmo --amsdos --alocal intro.asm intro.bin

clean:
	@rm -f *.bi*
	@rm -f *.cpc
	@rm -f *.cpr
	@rm -f *.sym
	@rm -f *.map
	@rm -f *.dsk

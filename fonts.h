#ifndef _FONTS_H
#define _FONTS_H

#asm
// The strings are not stored as ASCII but as index in the font array
//
// 00 01 02 03 04 05 06 07 08 09 10 11
//  0  1  2  3  4  5  6  7  8  9  A  B
// 12 13 14 15 16 17 18 19 20 21 22 23
//  C  D  E  F  G  H  I  J  K  L  M  N
// 24 25 26 27 28 29 30 31 32 33 34 35
//  O  P  Q  R  S  T  U  V  W  X  Y  Z
// 36 37 38 39 40 41 42 43 44
// co :   !  .  ,  '  ?  &

._fonts
defb  51, 255
defb 102, 119
defb 102, 255
defb 119, 187
defb 119,  51
defb 102,  51
defb 119, 238

defb  17, 204
defb   0, 204
defb   0, 204
defb   0, 204
defb   0, 204
defb   0, 204
defb  17, 238

defb 119, 238
defb   0,  51
defb   0,  51
defb 119, 255
defb 102,   0
defb 102,   0
defb 119, 255

defb 119, 238
defb   0,  51
defb   0,  51
defb  51, 255
defb   0,  51
defb   0,  51
defb 119, 255

defb 102,  51
defb 102,  51
defb 102,  51
defb 119, 255
defb   0,  51
defb   0,  51
defb   0,  51

defb 119, 255
defb 102,   0
defb 102,   0
defb 119, 238
defb   0,  51
defb   0,  51
defb 119, 238

defb  51, 255
defb 102,   0
defb 102,   0
defb 119, 255
defb 102,  51
defb 102,  51
defb 119, 238

defb 119, 255
defb   0,  51
defb   0,  51
defb   0,  51
defb   0,  51
defb   0,  51
defb   0,  51

defb 119, 238
defb 102,  51
defb 102,  51
defb 119, 255
defb 102,  51
defb 102,  51
defb  51, 255

defb 119, 238
defb 102,  51
defb 102,  51
defb 119, 255
defb   0,  51
defb   0,  51
defb   0,  51

defb   0, 119
defb   0, 255
defb  17, 187
defb  51,  51
defb 119, 255
defb 102,  51
defb 102,  51

defb 119, 238
defb 102,  51
defb 119, 238
defb 102,  51
defb 102,  51
defb 102,  51
defb 119, 238

defb  51, 255
defb 102,   0
defb 102,   0
defb 102,   0
defb 102,   0
defb 102,   0
defb  51, 255

defb 119, 238
defb   0,  51
defb   0,  51
defb 102,  51
defb 102,  51
defb 102,  51
defb 119, 238

defb 119, 255
defb   0,   0
defb   0,   0
defb 119, 238
defb 102,   0
defb 102,   0
defb 119, 255

defb 119, 255
defb   0,   0
defb   0,   0
defb 119, 238
defb 102,   0
defb 102,   0
defb 102,   0

defb  51, 238
defb 102,   0
defb 102,   0
defb 102, 255
defb 102,  51
defb 102,  51
defb  51, 255

defb 102,  51
defb 102,  51
defb 119, 255
defb 102,  51
defb 102,  51
defb 102,  51
defb 102,  51

defb 119, 255
defb  17, 136
defb  17, 136
defb  17, 136
defb  17, 136
defb  17, 136
defb 119, 255

defb 119, 255
defb   0,  51
defb   0,  51
defb 102,  51
defb 102,  51
defb 119, 255
defb  51, 238

defb 102, 102
defb 102, 204
defb 119, 238
defb 119,  51
defb 102,  51
defb 102,  51
defb 102,  51

defb 102,   0
defb 102,   0
defb 102,   0
defb 102,   0
defb 102,  51
defb 102,  51
defb 119, 255

defb 119, 238
defb 102, 187
defb 102, 187
defb 102, 187
defb 102,  51
defb 102,  51
defb 102,  51

defb 119, 238
defb 102,  51
defb 102,  51
defb 102,  51
defb 102,  51
defb 102,  51
defb 102,  51

defb  51, 238
defb 102,  51
defb 102,  51
defb 102,  51
defb 102,  51
defb 102,  51
defb  51, 238

defb 119, 255
defb 102,  51
defb 102,  51
defb 119, 238
defb 102,   0
defb 102,   0
defb 102,   0

defb  51, 238
defb 102,  51
defb 102,  51
defb 102,  51
defb 102, 255
defb 102, 119
defb  51, 255

defb 119, 238
defb 102,  51
defb 102,  51
defb 119, 255
defb 102, 204
defb 102, 102
defb 102,  51

defb  51, 238
defb 102,   0
defb 102,   0
defb 119, 255
defb   0,  51
defb   0,  51
defb 119, 238

defb 119, 255
defb  17, 136
defb  17, 136
defb  17, 136
defb  17, 136
defb  17, 136
defb  17, 136

defb 102,  51
defb 102,  51
defb 102,  51
defb 102,  51
defb 102,  51
defb 102,  51
defb  51, 255

defb 102,  51
defb 102,  51
defb 102,  51
defb  51,  51
defb  17, 187
defb   0, 255
defb   0, 119

defb 102,  51
defb 102,  51
defb 102, 187
defb 102, 187
defb 102, 187
defb 102, 187
defb  51, 255

defb 102,  51
defb  51, 102
defb  17, 204
defb  17, 204
defb  51, 102
defb 102,  51
defb 102,  51

defb 102,  51
defb 102,  51
defb 102,  51
defb 119, 255
defb   0,  51
defb   0,  51
defb 119, 238

defb 119, 255
defb   0,   0
defb   0, 102
defb   0, 204
defb  17, 136
defb  51,   0
defb 119, 255

defb   0,   0
defb 119, 238
defb 204, 187
defb 221,  51
defb 221, 187
defb 204,  51
defb 119, 238

defb   0,   0
defb   0, 204
defb   0, 204
defb   0,   0
defb   0, 204
defb   0, 204
defb   0,   0

defb   0, 204
defb   0, 204
defb   0, 204
defb   0, 136
defb   0, 136
defb   0,   0
defb  17, 136

defb   0,   0
defb   0,   0
defb   0,   0
defb   0,   0
defb   0,   0
defb  51,   0
defb  51,   0

defb   0,   0
defb   0,   0
defb   0,   0
defb   0,   0
defb  51,   0
defb  51,   0
defb 102,   0

defb  17, 136
defb  17, 136
defb  51,   0
defb   0,   0
defb   0,   0
defb   0,   0
defb   0,   0

defb 119, 238
defb 102, 119
defb   0, 119
defb  17, 238
defb  51, 136
defb   0,   0
defb  51,   0

defb  17, 204
defb  51, 102
defb  51, 204
defb 119, 238
defb 102, 204
defb 102, 102
defb  51, 187

// This the space, the empty tile and the explosion
._explosion4
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0
defb   0,   0,   0,   0,   0,   0

; number set_1 8x6 = 24bytes
.number_set1
defb    96, 128
defb   128, 128
defb   128, 128
defb   128, 128
defb   128, 128
defb   192,  32

defb    64,   0
defb   192,   0
defb    64,   0
defb    64,   0
defb    64,   0
defb   192, 128

defb    96,  32
defb   128, 128
defb     0, 128
defb    96,  32
defb   144,   0
defb   192, 128

defb   192,  32
defb     0, 128
defb    64,  32
defb     0, 128
defb     0, 128
defb   192,  32

defb    32, 128
defb   128, 128
defb   128, 128
defb   192, 128
defb     0, 128
defb     0, 128

defb   192, 128
defb   128,   0
defb   192,  32
defb     0, 128
defb     0, 128
defb   192,  32

defb    96, 128
defb   128,   0
defb   192,  32
defb   128, 128
defb   128, 128
defb   192, 128

defb   192, 128
defb     0, 128
defb     0, 128
defb    64,  32
defb   144,   0
defb   128,   0

defb    96, 128
defb   128, 128
defb    96,  32
defb   128, 128
defb   128, 128
defb   192,  32

defb    96, 128
defb   128, 128
defb   128, 128
defb   192, 128
defb     0, 128
defb   192,  32

._3dfont
; 3D letters
._3dfont_0
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79,  74, 133, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79,  74, 133, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_1
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 143, 197, 139
defb  79,  74, 197, 139
defb  79, 143, 197, 139
defb  79, 143, 197, 139
defb  79, 143, 197, 139
defb  79, 143, 197, 139
defb  79, 143, 197, 139
defb  79,  74, 192, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_2
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79,  74, 133, 139
defb  79, 197, 202, 139
defb  79, 207,  74, 139
defb  79, 143, 133, 139
defb  79,  74,  79, 139
defb  79, 133, 207, 139
defb  79, 197, 207, 139
defb  79, 192, 192, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_3
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79,  74, 133, 139
defb  79, 197, 202, 139
defb  79, 207, 202, 139
defb  79, 143, 133, 139
defb  79, 207, 202, 139
defb  79, 207, 202, 139
defb  79, 197, 202, 139
defb  79,  74, 133, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_4
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207,  74, 139
defb  79, 143, 192, 139
defb  79,  74,  74, 139
defb  79, 133, 202, 139
defb  79, 197, 202, 139
defb  79, 192, 192, 139
defb  79, 207, 202, 139
defb  79, 207, 202, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_5
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 192, 192, 139
defb  79, 197, 207, 139
defb  79, 197, 207, 139
defb  79, 192, 133, 139
defb  79, 207, 202, 139
defb  79, 207, 202, 139
defb  79, 207, 202, 139
defb  79, 192, 133, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_6
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79,  74, 133, 139
defb  79, 197, 202, 139
defb  79, 197, 207, 139
defb  79, 192, 133, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79,  74, 133, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_7
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 192, 192, 139
defb  79, 207, 202, 139
defb  79, 207, 202, 139
defb  79, 207,  74, 139
defb  79, 143, 133, 139
defb  79,  74,  79, 139
defb  79, 133, 207, 139
defb  79, 197, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_8
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79,  74, 133, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79,  74, 133, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79,  74, 133, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_9
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79,  74, 133, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79,  74, 192, 139
defb  79, 207, 202, 139
defb  79, 197, 202, 139
defb  79,  74, 133, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_A
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 202, 139
defb  79, 207,  74, 139
defb  79, 207, 192, 139
defb  79, 143, 192, 139
defb  79, 202,  74, 139
defb  79,  74, 192, 139
defb  79, 133, 202, 139
defb  79, 197, 202, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_E
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 143, 192, 139
defb  79, 202, 207, 139
defb  79,  74, 207, 139
defb  79,  74, 197, 139
defb  79, 133, 207, 139
defb  79, 197, 207, 139
defb  79, 197, 207, 139
defb  79,  74, 192, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_G
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 143, 192, 139
defb  79, 202,  79, 139
defb  79,  74, 207, 139
defb  79, 133, 207, 139
defb  79, 197, 192, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79,  74, 192, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_L
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 197, 139
defb  79, 143, 197, 139
defb  79, 202,  79, 139
defb  79, 202, 207, 139
defb  79,  74, 207, 139
defb  79, 133, 207, 139
defb  79, 197, 207, 139
defb  79, 192, 192, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_M
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79,  74, 143, 139
defb  79, 192,  74, 139
defb  79, 133, 192, 139
defb  79, 197,  74, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_O
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 143, 192, 139
defb  79, 202, 202, 139
defb  79,  74, 202, 139
defb  79, 133, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79,  74, 197, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_R
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 192, 133, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79, 192, 133, 139
defb  79, 197, 202, 139
defb  79, 197, 202, 139
defb  79,  79, 202, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_V
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79,  79, 143, 139
defb  79, 197,  74, 139
defb  79, 197, 133, 139
defb  79, 133, 197, 139
defb  79, 192,  79, 139
defb  79, 192, 207, 139
defb  79, 133, 207, 139
defb  79, 197, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont__
defb 212,  15,  15,  79
defb 237, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb  79, 207, 207, 139
defb 139,   3,   3,   3

._3dfont_t3
defb   0,  64, 128,   0
defb   0,  64, 128,   0
defb   0,  64, 168,   0
defb   0,  64, 168,   0
defb   0,  84, 168,   0
defb   0,  84,  10,   0
defb   0,  84, 168,   0
defb   0,  84,  10,   0
defb   0,  84, 168,   0
defb   0,  84,  10,   0
defb   0,  84, 168,   0
defb   0,  84,  10,   0
defb   0,   5, 138,   0
defb   0,  69,   2,   0

._3dfont_t2
defb   0, 197,   0,   0
defb   0, 133, 138,   0
defb   0, 133,  10,   0
defb   0, 133, 139,   0
defb   0, 173, 139,   0
defb   0, 133, 139,   0
defb   0, 133, 139,   0
defb   0, 173, 139,   0
defb   0,  15, 139,   0
defb   0, 173, 139,   0
defb   0,  79, 139,   0
defb   0, 173,   2,   0
defb   0,  79,   0,   0
defb   0,   3,   0,   0

._3dfont_t1
defb  64,  10,   0,   0
defb  64,  15, 138,   0
defb  84,  79, 143, 138
defb  84,  79,  79,   2
defb   5,  79, 197,   2
defb  84,  79, 197,   2
defb   5,  79, 197,   2
defb  84,  79, 197,   2
defb   5,  79, 133,   2
defb  84,  79, 197,   2
defb   5,  79,  79,   2
defb  84,  79, 139, 138
defb   5, 139, 138,   0
defb   1,   2,   0,   0

#endasm

#endif
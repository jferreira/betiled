; Intro
;--------------------------------------------------------------------------------------------------
; 1. Display CEZ logo
; 2. Display retroFactory
; 3. Display loading screen
;--------------------------------------------------------------------------------------------------

                org #3000

mapbasebits
                ds	156	                             ; tables for bits, baseL, baseH

cez
                incbin "cez.opt"

retro
                incbin "retro.opt"

screen
                incbin "screen.opt"

theend
                incbin "theend.opt"

start
                di
                ld hl,(#38)
                ld (vector),hl
                ld hl,#c9fb                         ; set interrupt vector
                ld (#38),hl
                ei

                ld hl,pallete_allblack
                call set_pal_mode0

                ld a,(#bfff)                        ; 128k flag
                or a
                jr nz,noendscr

                ld hl,theend
                ld de,#c000
                ld bc,start-theend
                ldir

                ld bc,#7fc7                         ; swapram7
                out (c),c
                ld hl,#c000
                ld de,#4000
                ld bc,start-theend
                ldir
                ld bc,#7fc0                         ; swapram7
                out (c),c

noendscr
                ld bc,#7f8c+1                       ; set mode 1
                out (c),c

                ld de,#c000
                ld hl,cez
                call uncrunch

                ld hl,pallete_cez
                call fade_in_mode1

                ld de,1000
loop1
                halt
                dec de
                ld a,e
                or d
                jr nz,loop1

                ld hl,pallete_cez
                call fade_out_mode1

                ld de,#c000
                ld hl,retro
                call uncrunch

                ld hl,pallete_retro
                call fade_in_mode1

                ld de,1000
loop2
                halt
                dec de
                ld a,e
                or d
                jr nz,loop2

                ld hl,pallete_retro
                call fade_out_mode1


                ld bc,#7f8c                         ; set mode 0
                out (c),c

                ld de,#c000
                ld hl,screen
                call uncrunch

                ld hl,pallete_screen
                call fade_in_mode0

                di
vector equ $+1
                ld hl,0                         ; set interrupt vector
                ld (#38),hl
                ei

                ret

;-------------------------------------------------------------------------------
; Exomizer 2 Z80 decoder
; by Metalbrain
;
; compression algorithm by Magnus Lind
;
; simple version:
;		no literal sequences
;		you MUST define mapbasebits aligned to a 256 boundary
;
; input:
;       hl=compressed data start
;		de=uncompressed destination start
uncrunch
                ld ixh,128
                ld b,52
                ld iy,mapbasebits
                push de

initbits
                ld a,b
	            sub	4
	            and	15
	            jr nz,node1
	            ld de,1		              ; DE=b2

node1
                ld c,16

get4bits
                call getbit
	            rl c
	            jr nc,get4bits
	            ld (iy+0),c	              ; bits[i]=b1

	            push hl
	            inc	c
	            ld hl,0
	            scf

setbit
                adc	hl,hl
	            dec	c
	            jr nz,setbit
	            ld (iy+52),e
	            ld (iy+104),d	          ; base[i]=b2
	            add	hl,de
	            ex de,hl
	            inc	iy

	            pop	hl
	            djnz initbits
	            pop	de

literalcopy
                ldi

mainloop
                call getbit	          ; literal?
	            jr c,literalcopy
	            ld c,255

getindex
                inc	c
	            call getbit
	            jr nc,getindex
	            ld a,c		              ; C=index
	            cp 16
	            ret	z
	            push de
	            call getpair
	            push bc
	            pop	af
	            ex af,af'		          ; lenght in AF
	            ld de,512+48	          ; 1?
	            dec	bc
	            ld a,b
	            or c
	            jr z,goforit
	            ld de,1024+32
	            dec	bc		              ; 2?
	            ld a,b
	            or c
	            jr z,goforit
	            ld e,16

goforit
                call getbits
	            ld a,e
	            add	a,c
	            ld c,a
	            call getpair	      ; bc=offset
	            pop	de		              ; de=destination
	            push hl
	            ld h,d
	            ld l,e
	            sbc	hl,bc		          ; hl=origin
	            ex af,af'
	            push af
	            pop	bc		              ; bc=lenght
	            ldir
	            pop	hl		              ; Keep HL, DE is updated
	            jr mainloop	          ; Next!

getpair
                ld iyl,c
	            ld d,(iy+0)
	            call getbits
	            ld a,c
	            add	a,(iy+52)
	            ld c,a
	            ld a,b
	            adc	a,(iy+104)	          ; Always clear C flag
	            ld b,a
	            ret

getbits
                ld bc,0		              ; get D bits in BC

gettingbits
                dec	d
	            ret	m
	            call getbit
	            rl c
	            rl b
	            jr gettingbits

getbit
                ld a,ixh		          ; get one bit
	            add	a,a
	            ld ixh,a
	            ret	nz
	            ld a,(hl)
	            inc	hl
	            rla
	            ld ixh,a
	            ret


;-------------------------------------------------------------------------------
pallete_screen
                db #54,#54,#4f,#5a,#55,#4b,#56,#40,#5b,#43,#5e,#5f,#4c,#5d,#4e,#5c,#54
                db #54,#54,#47,#5e,#44,#43,#54,#5e,#59,#4a,#5c,#40,#5c,#58,#4c,#54,#54
                db #54,#54,#4e,#5c,#54,#4a,#54,#5c,#5a,#4e,#54,#5e,#54,#5c,#5c,#54,#54
                db #54,#54,#4c,#54,#54,#4e,#54,#54,#5e,#5c,#54,#5c,#54,#54,#54,#54,#54
                db #54,#54,#5c,#54,#54,#5c,#54,#54,#5c,#54,#54,#54,#54,#54,#54,#54,#54
pallete_allblack
                db #54,#54,#54,#54,#54,#54,#54,#54,#54,#54,#54,#54,#54,#54,#54,#54,#54

pallete_cez
                db #4b,#53,#5d,#57,#4b
                db #43,#42,#58,#46,#43
                db #4a,#52,#5c,#56,#4a
                db #4e,#56,#54,#54,#4e
                db #5c,#54,#54,#54,#5c
                db #54,#54,#54,#54,#54

pallete_retro
                db #4b,#46,#4e,#54,#4b
                db #43,#56,#4c,#54,#43
                db #4a,#54,#5c,#54,#4a
                db #4e,#54,#54,#54,#4e
                db #5c,#54,#54,#54,#5c
                db #54,#54,#54,#54,#54

;-------------------------------------------------------------------------------
; Fade in
; hl = pallete to use
fade_in_mode1
                ld a,5

_loop
                push hl
                call fade_mode1
                pop hl
                dec a
                cp #ff
                jr nz,_loop
                ret
;-------------------------------------------------------------------------------
; Fade in
; hl = pallete to use
fade_in_mode0
                ld a,5

_loop
                push hl
                call fade_mode0
                pop hl
                dec a
                cp #ff
                jr nz,_loop
                ret

;-------------------------------------------------------------------------------
; Fade out
fade_out_mode1
                xor a

_loop
                push hl
                call fade_mode1
                pop hl
                inc a
                cp 6
                jr nz,_loop
                ret

;-------------------------------------------------------------------------------
fade_mode1
                push af
                ld b,18

_loop
                ei
                halt
                djnz _loop

                ex af,af'
                call vsync
                ex af,af'

                ld d,a
                add a,a
                add a,a
                add a,d
                ld d,0
                ld e,a
                add hl,de
                call set_pal_mode1
                pop af
                ret

;-------------------------------------------------------------------------------
fade_mode0
                push af
                ld b,18

_loop
                ei
                halt
                djnz _loop

                ex af,af'
                call vsync
                ex af,af'

                ld d,a
                add a,a
                add a,a
                add a,a
                add a,a
                add a,d
                ld d,0
                ld e,a
                add hl,de
                call set_pal_mode0
                pop af
                ret

;-------------------------------------------------------------------------------
set_pal_mode0
                ld bc,$7f11        ; #7fxx GATE ARRAY IO port
                jr set_pal

set_pal_mode1
                ld bc,$7f10
                ld a,(hl)          ; HL = pointer to palette
                inc hl
                out (c),c          ; Select PEN
                out (c),a          ; Select color for selected pen

                ld bc,$7f04
set_pal
                ld a,(hl)          ; HL = pointer to palette
                inc hl
                dec c
                out (c),c          ; Select PEN
                out (c),a          ; Select color for selected pen
                jr nz,set_pal
                ret

;-------------------------------------------------------------------------------
vsync:
                ld b,#f5

vsync_loop:
                in a,(c)
                rra
                jr nc,vsync_loop
                ret
end start